/tmp/g/tools/dsnet/dbg/exe.c:
struct brkpt { /* size 32*/
  struct brkpt *forw; /* bitsize 32, bitpos 0 */
  struct brkpt *back; /* bitsize 32, bitpos 32 */
  int flag; /* bitsize 32, bitpos 64 */
  int no; /* bitsize 32, bitpos 96 */
  int init_cnt; /* bitsize 32, bitpos 128 */
  int curr_cnt; /* bitsize 32, bitpos 160 */
  word adr; /* bitsize 32, bitpos 192 */
  word ins; /* bitsize 32, bitpos 224 */
};
typedef struct brkpt BRKPT;
static int brkpt_no;
static int get_target_brkpt (int flag /* 0x8 */)
{
  {
    int nc;
    int i;
    int r;
    BRKPT *bp;
    DBGP_BRKPT_DATA *cbps;
  }
}
static int put_target_brkpt (int flag /* 0x8 */)
{
  {
    int nr;
    int i;
    int r;
    BRKPT *bp;
    DBGP_BRKPT_DATA *rbps;
  }
}
static int store_breakpoints (int flag /* 0x8 */)
{
  {
    word break_ins;
    BRKPT *bp;
  }
}
static int restore_breakpoints (int flag /* 0x8 */)
{
  {
    word break_ins;
    word ins;
    BRKPT *bp;
  }
}
int store_user_breakpoints ()
{
}
int restore_user_breakpoints ()
{
}
int eval_bp_reg (char *name /* 0x8 */, word *padr /* 0xc */)
{
  {
    word wv;
    BRKPT *bp;
  }
}
static void print_brkpt (BRKPT *bp /* 0x8 */, char *msg /* 0xc */)
{
  {
    char buf[1024];
  }
}
static int add_brkpt (int flag /* 0x8 */, word adr /* 0xc */, word cnt /* 0x10 */)
{
  {
    BRKPT *bp;
  }
}
static int del_brkpt (int flag /* 0x8 */, word adr /* 0xc */)
{
  {
    int r;
    BRKPT *bq;
    BRKPT *bp;
  }
}
int remove_breakpoints ()
{
  {
    BRKPT *bp;
  }
}
int bp_cmd (int ac /* 0x8 */, char **av /* 0xc */)
{
  {
    char *p;
    char buf[1024];
    int flag;
    int n;
    word cnt;
    word adr;
    BRKPT *bp;
  }
}
static void automatic_prefix_breakpoint (char *dp /* 0x8 */, char *sp /* 0xc */)
{
  {
    char *p;
    {
      char _c;
    }
  }
}
int ub_cmd (int ac /* 0x8 */, char **av /* 0xc */)
{
  {
    int n;
    char buf[1024];
    word adr;
  }
}
int be_cmd (int ac /* 0x8 */, char **av /* 0xc */)
{
  {
    char buf[1024];
    int n;
    int f_bd;
    word adr;
    BRKPT *bp;
  }
}
int bd_cmd (int ac /* 0x8 */, char **av /* 0xc */)
{
}
int break_cmd (int ac /* 0x8 */, char **av /* 0xc */)
{
  {
    int r;
  }
}
int wait_cmd (int ac /* 0x8 */, char **av /* 0xc */)
{
  {
    int r;
  }
}
static int targ_cont_cmd (int ac /* 0x8 */, char **av /* 0xc */)
{
  {
    int r;
  }
}
static int targ_until_cmd (int ac /* 0x8 */, char **av /* 0xc */)
{
  {
    int i;
    int r;
    word adr;
  }
}
static int targ_step_cmd (int ac /* 0x8 */, char **av /* 0xc */)
{
  {
    int r;
    int n;
    int cnt;
    static int last_cnt;
  }
}
static int targ_next_cmd (int ac /* 0x8 */, char **av /* 0xc */)
{
  {
    int r;
    int n;
    int cnt;
    static int last_cnt;
  }
}
static int targ_lstep_cmd (int ac /* 0x8 */, char **av /* 0xc */)
{
  {
    char *file1;
    char *file0;
    word adr1;
    word adr0;
    int delta1;
    int delta0;
    int line1;
    int line0;
    int r;
    int cnt;
    static int last_cnt;
  }
}
static int targ_lnext_cmd (int ac /* 0x8 */, char **av /* 0xc */)
{
  {
    char *file1;
    char *file0;
    word adr1;
    word adr0;
    int delta1;
    int delta0;
    int line1;
    int line0;
    int r;
    int cnt;
    static int last_cnt;
  }
}
static int targ_luntil_cmd (int ac /* 0x8 */, char **av /* 0xc */)
{
  {
    char *file0;
    word addr;
    int delta0;
    int line0;
    int r;
    int line;
  }
}
typedef struct { /* size 16 */
  word cause; /* bitsize 32, bitpos 0 */
  word epc; /* bitsize 32, bitpos 32 */
  word status; /* bitsize 32, bitpos 64 */
  word errorepc; /* bitsize 32, bitpos 96 */
} SCRS;
//TODO: Replace for IOP >>
typedef struct { /* size 8 */
  word cause; /* bitsize 32, bitpos 0 */
  word epc; /* bitsize 32, bitpos 32 */
} SCRS;
//<<
static word get_pc (SCRS *scrs /* 0x8 */)
{
}
static int is_breakpoint (int flag /* 0x8 */, SCRS *scrs /* 0xc */)
{
  {
    word adr;
    BRKPT *p;
  }
}
static int load_scrs (SCRS *scrs /* 0x8 */)
{
  {
    quad vals[4];
    word masks[11];
    //TODO: Replace for IOP >>
    word vals[2];
    word masks[10];
    //<<
  }
}
static int do_cont (int flag /* 0x8 */, SCRS *scrs /* 0xc */)
{
  {
    int r;
  }
}
static int host_do_step (int f_next /* 0x8 */, SCRS *scrs /* 0xc */)
{
  {
    word rpc;
    word tadr;
    word ins;
  }
}
void display_current_informations (int result /* 0x8 */)
{
}
static int do_lstep (int f_next /* 0x8 */, SCRS *scrs /* 0xc */)
{
  {
    int delta1;
    int delta0;
    int line1;
    int line0;
    int r;
    char *file1;
    char *file0;
    word adr1;
    word adr0;
  }
}
static int host_cont_cmd (int ac /* 0x8 */, char **av /* 0xc */)
{
  {
    int r;
    static int last_cnt;
    SCRS *scrs;
    SCRS scrs_area;
    int i;
    int cnt;
  }
}
static int host_until_cmd (int ac /* 0x8 */, char **av /* 0xc */)
{
  {
    int i;
    int r;
    word adrs[100];
    SCRS *scrs;
    SCRS scrs_area;
  }
}
static int host_step_cmd (int ac /* 0x8 */, char **av /* 0xc */)
{
  {
    int r;
    static int last_cnt;
    SCRS *scrs;
    SCRS scrs_area;
    int i;
    int cnt;
    int f_next;
  }
}
static int host_next_cmd (int ac /* 0x8 */, char **av /* 0xc */)
{
  {
    int r;
    static int last_cnt;
    SCRS *scrs;
    SCRS scrs_area;
    int i;
    int cnt;
    int f_next;
  }
}
static int host_lstep_cmd (int ac /* 0x8 */, char **av /* 0xc */)
{
  {
    int r;
    static int last_cnt;
    SCRS *scrs;
    SCRS scrs_area;
    int i;
    int cnt;
    int f_next;
  }
}
static int host_lnext_cmd (int ac /* 0x8 */, char **av /* 0xc */)
{
  {
    int r;
    static int last_cnt;
    SCRS *scrs;
    SCRS scrs_area;
    int i;
    int cnt;
    int f_next;
  }
}
static int host_luntil_cmd (int ac /* 0x8 */, char **av /* 0xc */)
{
  {
    int r;
    static int last_cnt;
    char *file0;
    SCRS *scrs;
    SCRS scrs_area;
    word delta0;
    word line0;
    word line;
    word addr;
    int i;
    int cnt;
  }
}
static int run_argc;
static char *run_args;
static int run_args_len;
int set_runarg (int ac /* 0x8 */, char **av /* 0xc */)
{
  {
    char *buf;
    char *bp;
    int i;
  }
}
int run_cmd (int ac /* 0x8 */, char **av /* 0xc */)
{
  {
    quad qv;
    int r;
    word entry;
  }
}
int cont_cmd (int ac /* 0x8 */, char **av /* 0xc */)
{
}
int until_cmd (int ac /* 0x8 */, char **av /* 0xc */)
{
}
int step_cmd (int ac /* 0x8 */, char **av /* 0xc */)
{
}
int next_cmd (int ac /* 0x8 */, char **av /* 0xc */)
{
}
int lstep_cmd (int ac /* 0x8 */, char **av /* 0xc */)
{
}
int lnext_cmd (int ac /* 0x8 */, char **av /* 0xc */)
{
}
int luntil_cmd (int ac /* 0x8 */, char **av /* 0xc */)
{
}
int (*ds_rdwr_mem_align_func) (/* unknown */);
int (*ds_load_mem_func) (/* unknown */);
int (*ds_store_mem_func) (/* unknown */);
int (*ds_load_quad_reg_func) (/* unknown */);
int (*ds_store_quad_reg_func) (/* unknown */);
int (*ds_symbol_to_value_func) (/* unknown */);
int (*ds_symbol_completion_func) (/* unknown */);
int (*ds_help_completion_func) (/* unknown */);
void (*ds_idle_func) (/* unknown */);
int (*ds_check_reserved_name_func) (/* unknown */);
void (*ds_drfp_err_func) (/* unknown */);
static struct { /* size 8 */
  BRKPT *head; /* bitsize 32, bitpos 0 */
  BRKPT *tail; /* bitsize 32, bitpos 32 */
} bps;
static int exitc (int r /* 0x8 */)
{
  register int r;
}
