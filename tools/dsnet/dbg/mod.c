/tmp/g/tools/dsnet/dbg/mod.c:
struct imod { /* size 244*/
  struct imod *forw; /* bitsize 32, bitpos 0 */
  struct imod *back; /* bitsize 32, bitpos 32 */
  int flags; /* bitsize 32, bitpos 64 */
  word id; /* bitsize 32, bitpos 96 */
  ILOADP_MODINFO_DATA info; /* bitsize 256, bitpos 128 */
//TODO: additional for EE/ERX >>
  word erx_stub_addr; /* bitsize 32, bitpos 384 */
  word erx_stub_size; /* bitsize 32, bitpos 416 */
  word erx_lib_addr; /* bitsize 32, bitpos 448 */
  word erx_lib_size; /* bitsize 32, bitpos 480 */
//<<
  char *name; /* bitsize 32, bitpos 512 */
  byte status; /* bitsize 8, bitpos 544 */
  word retval; /* bitsize 32, bitpos 576 */
  int arg_siz; /* bitsize 32, bitpos 608 */
  char args[160]; /* bitsize 1280, bitpos 640 */
  word vaddr; /* bitsize 32, bitpos 1920 */
};
typedef struct imod IMOD;
static struct { /* size 8 */
  IMOD *head; /* bitsize 32, bitpos 0 */
  IMOD *tail; /* bitsize 32, bitpos 32 */
} imod_list;
static IMOD *last_imod;
static word *mblks;
static word *mblke;
static int mod_id_by_name_internal (int id /* 0x8 */, char *name /* 0xc */, int ver /* 0x10 */)
{
  {
    IMOD *p;
    half ver;
  }
}
int mod_id_by_name (int id /* 0x8 */, char *name /* 0xc */, int ver /* 0x10 */)
{
  {
    int r;
    IMOD *p;
    half ver;
  }
}
int mod_fetch_id (int id /* 0x8 */)
{
  {
    IMOD *p;
  }
}
word mod_address_by_id (int id /* 0x8 */)
{
  {
    IMOD *p;
  }
}
void mod_set_vaddr (int id /* 0x8 */, word vaddr /* 0xc */)
{
  {
    IMOD *p;
  }
}
static IMOD *look_imod (word id /* 0x8 */)
{
  {
    IMOD *p;
  }
}
static IMOD *alloc_imod (int id /* 0x8 */, int ac /* 0xc */, char **av /* 0x10 */)
{
  {
    int i;
    int f_dev;
    char *buf;
    char *bp;
    IMOD *q;
    IMOD *p;
  }
}
static IMOD *free_imod (IMOD *p /* 0x8 */)
{
}
void clear_mod_list ()
{
  {
    IMOD *q;
    IMOD *p;
  }
}
static void set_modname (IMOD *p /* 0x8 */, char *name /* 0xc */)
{
}
static void load_mod_symbols (IMOD *p /* 0x8 */)
{
  {
    char *dp;
    char *sp;
    char path[1024];
    char buf[1024];
  }
}
int iload_callback (word id /* 0x8 */, int cmd /* 0xc */, void *ptr /* 0x10 */, int len /* 0x14 */)
{
  {
    word *ide;
    word *ids;
    ILOADP_MODINFO_DATA *info;
    IMOD *p;
    byte cmd;
  }
}
int mload_cmd (int ac /* 0x8 */, char **av /* 0xc */)
{
  {
    int action;
    int r;
    IMOD *p;
  }
}
int mstart_cmd (int ac /* 0x8 */, char **av /* 0xc */)
{
  {
    word id;
    int f_debug;
    int action;
    int r;
    IMOD *q;
    IMOD *p;
  }
}
int mremove_cmd (int ac /* 0x8 */, char **av /* 0xc */)
{
  {
    word id;
    IMOD *q;
    IMOD *p;
  }
}
static void print_id (char *str /* 0x8 */, IMOD *p /* 0xc */)
{
}
static void print_addr (char *str /* 0x8 */, IMOD *p /* 0xc */)
{
}
static void print_size (char *str /* 0x8 */, IMOD *p /* 0xc */)
{
}
static void print_last_addr (char *str /* 0x8 */, IMOD *p /* 0xc */)
{
}
static void print_vers (char *str /* 0x8 */, IMOD *p /* 0xc */)
{
}
static void print_name (char *str /* 0x8 */, IMOD *p /* 0xc */)
{
}
static void print_flags (char *str /* 0x8 */, IMOD *p /* 0xc */)
{
}
static void print_entry (char *str /* 0x8 */, IMOD *p /* 0xc */)
{
}
static void print_gp (char *str /* 0x8 */, IMOD *p /* 0xc */)
{
}
static void print_tsize (char *str /* 0x8 */, IMOD *p /* 0xc */)
{
}
static void print_dsize (char *str /* 0x8 */, IMOD *p /* 0xc */)
{
}
static void print_bsize (char *str /* 0x8 */, IMOD *p /* 0xc */)
{
}
static void print_erxstub (char *str /* 0x8 */, IMOD *p /* 0xc */)
{
}
static void print_erxlib (char *str /* 0x8 */, IMOD *p /* 0xc */)
{
}
static void print_args (IMOD *p /* 0x8 */)
{
  {
    int na;
    int n;
    char *pa;
  }
}
static void print_status (char *fmt /* 0x8 */, IMOD *p /* 0xc */)
{
}
static void print_retval (char *fmt /* 0x8 */, IMOD *p /* 0xc */)
{
}
int mlist_cmd (int ac /* 0x8 */, char **av /* 0xc */)
{
  {
    int f_long;
    int f_mem;
    int r;
    IMOD *q;
    IMOD *p;
  }
}
int memlist_cmd (int ac /* 0x8 */, char **av /* 0xc */)
{
  {
    word range;
    word adr;
    word *wp;
    int f_adr;
    int f_free;
    int f_sup;
    int r;
    IMOD *q;
    IMOD *p;
  }
}
int (*ds_rdwr_mem_align_func) (/* unknown */);
int (*ds_load_mem_func) (/* unknown */);
int (*ds_store_mem_func) (/* unknown */);
int (*ds_load_quad_reg_func) (/* unknown */);
int (*ds_store_quad_reg_func) (/* unknown */);
int (*ds_symbol_to_value_func) (/* unknown */);
int (*ds_symbol_completion_func) (/* unknown */);
int (*ds_help_completion_func) (/* unknown */);
void (*ds_idle_func) (/* unknown */);
int (*ds_check_reserved_name_func) (/* unknown */);
void (*ds_drfp_err_func) (/* unknown */);
