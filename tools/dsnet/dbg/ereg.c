/tmp/g/tools/dsnet/dbg/ereg.c:
typedef struct { /* size 16 */
  int kind; /* bitsize 32, bitpos 0 */
  int number; /* bitsize 32, bitpos 32 */
  int size; /* bitsize 32, bitpos 64 */
  char *name; /* bitsize 32, bitpos 96 */
} EE_REGS;
static EE_REGS ee_regs[];
static void print_symcr (word rcr /* 0x8 */)
{
}
static void print_rcr (word rcr /* 0x8 */)
{
}
static void print_symsr (word rsr /* 0x8 */)
{
}
static void print_rsr (word rsr /* 0x8 */)
{
}
static void print_bpc (word bpc /* 0x8 */)
{
}
static void print_vuc (char *name /* 0x8 */, int kind /* 0xc */, int number /* 0x10 */, word val /* 0x14 */)
{
  {
    EE_REGS *reg;
  }
}
static int set_regmasks (word *masks /* 0x8 */, char *name /* 0xc */)
{
  {
    int rk;
    int n;
    EE_REGS *reg;
    char *p;
    {
      char _c;
    }
  }
}
int check_reserved_name (char *name /* 0x8 */)
{
  {
    EE_REGS *reg;
  }
}
int load_quad_reg (char *name /* 0x8 */, quad *pv /* 0xc */)
{
  {
    int j;
    int i;
    int r;
    quad vals[4];
    word wv;
    word rerpc;
    word repc;
    word rcr;
    word rsr;
    word masks[11];
  }
}
int store_quad_reg (char *name /* 0x8 */, quad val /* 0xc */)
{
  {
    int n;
    int i;
    quad vals[352];
    word masks[11];
  }
}
static int dr_default_setmask (word *masks /* 0x8 */, int kind /* 0xc */, int number /* 0x10 */)
{
}
static int dr_default (char *fmt /* 0x8 */, word *masks /* 0xc */, quad *vals /* 0x10 */)
{
  {
    int ch;
    int nreg;
    int n;
    EE_REGS *reg;
    char *p;
    {
      char _c;
    }
    {
      char _c;
    }
    {
      char _c;
    }
    {
      word rpc;
      word rerpc;
      word repc;
      word rcr;
      word rsr;
    }
    {
      char _c;
    }
    {
      char _c;
    }
    {
      char _c;
    }
  }
}
int dreg_cmd (int ac /* 0x8 */, char **av /* 0xc */)
{
  {
    char *p;
    int cpuid;
    int f_xyzw;
    int f_float;
    int n;
    int i;
    int r;
    EE_REGS *reg;
    quad *pv;
    quad vals[352];
    word masks[11];
    {
      char *fmt;
      word rpc;
      word rerpc;
      word rbadp;
      word repc;
      word rcr;
      word rsr;
      word rbadv;
      quad *gpr;
      quad *hilo;
      {
        int _n;
      }
      {
        int _n;
      }
    }
    {
      int vun;
      int kind;
      {
        int _n;
      }
      {
        int _n;
      }
    }
    {
      float fv;
    }
    {
      quad q32;
      quad qv;
      {
        float fx;
        float fy;
        float fz;
        float fw;
      }
      {
        word wx;
        word wy;
        word wz;
        word ww;
      }
    }
  }
}
int sreg_cmd (int ac /* 0x8 */, char **av /* 0xc */)
{
  {
    int f_force;
    quad val;
  }
}
int hbp_cmd (int ac /* 0x8 */, char **av /* 0xc */)
{
  {
    int f_debug;
    int hub;
    word vmsk;
    word val;
    word amsk;
    word adr;
    char *q;
    char *p;
    char vmsks[1024];
    char vals[1024];
    char amsks[1024];
    char adrs[1024];
    char type[1024];
    quad regs[7];
    word dvbm;
    word dvb;
    word dabm;
    word dab;
    word iabm;
    word iab;
    word bpc;
    word masks[11];
  }
}
int rload_cmd (int ac /* 0x8 */, char **av /* 0xc */)
{
  {
    int n;
    int siz;
    int r;
    EE_REGS *reg;
    quad val;
    quad tvals[352];
    quad avals[352];
    word tmasks[11];
    word amasks[11];
    DS_FILE *stream;
    char vstr[1024];
    char rstr[1024];
    char *q;
    char *pe;
    char *p;
    char *buf;
    char *fname;
    {
      char _c;
    }
  }
}
int rsave_cmd (int ac /* 0x8 */, char **av /* 0xc */)
{
  {
    int n;
    int r;
    EE_REGS *reg;
    quad vals[352];
    word masks[11];
    DS_FILE *stream;
    char buf[1024];
    char *fname;
    {
      int _n;
    }
    {
      int _n;
    }
  }
}
int (*ds_rdwr_mem_align_func) (/* unknown */);
int (*ds_load_mem_func) (/* unknown */);
int (*ds_store_mem_func) (/* unknown */);
int (*ds_load_quad_reg_func) (/* unknown */);
int (*ds_store_quad_reg_func) (/* unknown */);
int (*ds_symbol_to_value_func) (/* unknown */);
int (*ds_symbol_completion_func) (/* unknown */);
int (*ds_help_completion_func) (/* unknown */);
void (*ds_idle_func) (/* unknown */);
int (*ds_check_reserved_name_func) (/* unknown */);
void (*ds_drfp_err_func) (/* unknown */);
