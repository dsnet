/tmp/g/tools/dsnet/dbg/opc.c:
int is_branch_instruction (word adr /* 0x8 */, word ins /* 0xc */, word *ptadr /* 0x10 */)
{
  {
    quad qv;
    char tmp[10];
  }
}
char *gpr_name[];
static char *cpr_name[][32];
static char *ccr_name[][32];
static char *dest_name[];
static char *xyzw_name[];
static char *detect_load_32bits (char *bp /* 0x8 */, word adr /* 0xc */, word inst /* 0x10 */, word next /* 0x14 */)
{
  {
    char buf[1024];
    word imm1;
    word imm0;
    word rt0;
  }
}
static int tabs (char *buf /* 0x8 */, char *bp /* 0xc */)
{
}
static char *func_and_line (char *bp /* 0x8 */, word adr /* 0xc */, int kind /* 0x10 */)
{
  {
    int f_has;
    int line;
    char *file;
  }
}
static char *source_line (word adr /* 0x8 */, char **pfile /* 0xc */, int *pline /* 0x10 */)
{
  {
    int line;
    char *obj_path;
    char *file;
  }
}
static char *disasm (char *buf /* 0x8 */, word adr /* 0xc */, word prev /* 0x10 */, word inst /* 0x14 */, word next /* 0x18 */, int f_adr /* 0x1c */, int f_iw /* 0x20 */, int f_ba /* 0x24 */, int f_ma /* 0x28 */)
{
  {
    int rd;
    int rt;
    int rs;
    int simm;
    int si;
    int cm;
    int v;
    char ch;
    char *dp;
    char *sp;
    char opr[1024];
    char *bp;
    OPCODE *opcode;
    {
      char _c;
    }
    {
      char _c;
    }
    {
      char _c;
    }
  }
}
static void display_symbol_once (char *old /* 0x8 */, word adr /* 0xc */, int f_iw /* 0x10 */, int f_adr /* 0x14 */)
{
  {
    int line;
    char *sl;
    char fl_buf[1024];
    char *q;
    char *p;
    char new[1024];
    char buf[1024];
  }
}
int di_cmd (int ac /* 0x8 */, char **av /* 0xc */)
{
  {
    static int last_cnt;
    char *sl;
    char sym[1024];
    int line;
    int f_ma;
    int f_ba;
    int f_iw;
    int f_adr;
    int f_mark;
    int mark;
    int len;
    int cnt;
    int i;
    int adr;
    word next;
    word inst;
    word prev;
    byte *dp;
    byte dat[65507];
    byte buf[1024];
  }
}
static int name_regno (int kind /* 0x8 */, int z /* 0xc */, char *name /* 0x10 */, int *pval /* 0x14 */)
{
  {
    int val;
    {
      char _c;
    }
  }
}
static int assemble_pseudo_instruction (char *name /* 0x8 */, char *bp /* 0xc */, word *ovals /* 0x10 */, int olen /* 0x14 */)
{
  {
    int i;
    int siz;
    int val;
    byte buf[1024];
    char *p;
    char *op;
    char opr[1024];
    {
      char _c;
    }
  }
}
static int assemble (char *buf /* 0x8 */, word *ovals /* 0xc */, int olen /* 0x10 */, int f_ma /* 0x14 */)
{
  {
    int dest;
    int bc;
    int v;
    word inst;
    char ch;
    char *fmt;
    char *op;
    char *tp;
    char opr[1024];
    char tok[1024];
    char *bp;
    OPCODE *opcode;
    {
      char _c;
    }
    {
      char _c;
    }
    {
      char _c;
    }
    {
      char _c;
    }
    {
      char _c;
    }
    {
      char _c;
    }
    {
      char _c;
    }
    {
      char _c;
    }
    {
      char _c;
    }
    {
      char _c;
    }
  }
}
int as_cmd (int ac /* 0x8 */, char **av /* 0xc */)
{
  {
    int f_ma;
    char buf[1024];
  }
}
struct bt { /* size 24*/
  struct bt *forw; /* bitsize 32, bitpos 0 */
  struct bt *back; /* bitsize 32, bitpos 32 */
  int no; /* bitsize 32, bitpos 64 */
  word adr; /* bitsize 32, bitpos 96 */
  word sp_lo; /* bitsize 32, bitpos 128 */
  word sp_hi; /* bitsize 32, bitpos 160 */
};
typedef struct bt BT;
static struct { /* size 8 */
  BT *head; /* bitsize 32, bitpos 0 */
  BT *tail; /* bitsize 32, bitpos 32 */
} bts;
static int bt_no;
static void clear_bts ()
{
  {
    BT *q;
    BT *p;
  }
}
static BT *append_bt (word adr /* 0x8 */, word sp /* 0xc */)
{
  {
    BT *p;
  }
}
int eval_bt_reg (char *name /* 0x8 */, word *padr /* 0xc */)
{
  {
    word wv;
    BT *bt;
  }
}
int eval_sfa_reg (char *name /* 0x8 */, word *padr /* 0xc */)
{
  {
    word wv;
    BT *bt;
  }
}
int eval_sfs_reg (char *name /* 0x8 */, word *padr /* 0xc */)
{
  {
    word wv;
    BT *bt;
  }
}
static int bt_fetch_word (word adr /* 0x8 */, word *pval /* 0xc */)
{
  {
    word val;
  }
}
static int backtrace_to_end_of_prologue (byte *ins_buf /* 0x8 */, word adr0 /* 0xc */, BT_REG *br /* 0x10 */, int level /* 0x14 */)
{
  {
    word ins;
    word adr;
  }
}
static int backtrace_to_top_of_function (byte *ins_buf /* 0x8 */, word adr0 /* 0xc */, BT_REG *br /* 0x10 */, BT *bt /* 0x14 */)
{
  {
    word high;
    word ins;
    word adr;
  }
}
static int backtrace_to_prev_function (BT_REG *br /* 0x8 */)
{
}
static word address_to_top_of_function (BT_REG *br /* 0x8 */, int level /* 0xc */, int f_mod /* 0x10 */)
{
  {
    int n;
    word high;
    word ins;
    word addu;
    word adr;
    word adr1;
    word adr0;
    byte *ins_buf;
  }
}
void disp_bt (BT_REG *br /* 0x8 */, int cnt /* 0xc */, int f_mod /* 0x10 */)
{
  {
    word old_ra;
    word adr1;
    word adr0;
    BT *bt;
    char buf[1024];
    byte *ins_buf;
    int n;
    int i;
  }
}
int bt_cmd (int ac /* 0x8 */, char **av /* 0xc */)
{
  {
    BT_REG br;
    int f_mod;
    int cnt;
  }
}
int (*ds_rdwr_mem_align_func) (/* unknown */);
int (*ds_load_mem_func) (/* unknown */);
int (*ds_store_mem_func) (/* unknown */);
int (*ds_load_quad_reg_func) (/* unknown */);
int (*ds_store_quad_reg_func) (/* unknown */);
int (*ds_symbol_to_value_func) (/* unknown */);
int (*ds_symbol_completion_func) (/* unknown */);
int (*ds_help_completion_func) (/* unknown */);
void (*ds_idle_func) (/* unknown */);
int (*ds_check_reserved_name_func) (/* unknown */);
void (*ds_drfp_err_func) (/* unknown */);
