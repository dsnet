 dbg.h:
typedef struct { /* size 16 */
  char *fmt; /* bitsize 32, bitpos 0 */
  word mask; /* bitsize 32, bitpos 32 */
  word code; /* bitsize 32, bitpos 64 */
  word isa; /* bitsize 32, bitpos 96 */
} OPCODE;
typedef struct { /* size 16 */
  word pc; /* bitsize 32, bitpos 0 */
  word sp; /* bitsize 32, bitpos 32 */
  word fp; /* bitsize 32, bitpos 64 */
  word ra; /* bitsize 32, bitpos 96 */
} BT_REG;
