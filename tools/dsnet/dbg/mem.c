/tmp/g/tools/dsnet/dbg/mem.c:
int dmem_cmd (int ac /* 0x8 */, char **av /* 0xc */)
{
  {
    char *av0;
    static int last_cnt;
    static int last_sz;
    float fv;
    word wv;
    int n;
    int sz;
    int j;
    int i;
    int cnt;
    byte align;
    byte *bp;
    byte *buf;
  }
}
int smem_cmd (int ac /* 0x8 */, char **av /* 0xc */)
{
  {
    void *ptr;
    float fv;
    quad qv;
    twin tv;
    word wv;
    half hv;
    byte bv;
    byte align;
    int i;
    int n;
    int sz;
    char *av0;
    char buf[1024];
  }
}
int inp_cmd (int ac /* 0x8 */, char **av /* 0xc */)
{
  {
    char *av0;
    static int last_sz;
    static int last_cnt;
    static int last_adr;
    int n;
    int sz;
    int j;
    int i;
    int cnt;
    int adr;
    byte align;
    byte *bp;
    byte *buf;
  }
}
int out_cmd (int ac /* 0x8 */, char **av /* 0xc */)
{
  {
    void *ptr;
    quad qv;
    twin tv;
    word wv;
    half hv;
    byte bv;
    byte align;
    int i;
    int n;
    int sz;
    int adr;
    char *av0;
    char buf[1024];
  }
}
static void show_statics (int tb /* 0x8 */, int mst /* 0xc */, int mut /* 0x10 */, int tst /* 0x14 */, int tut /* 0x18 */)
{
  {
    int tbps;
    int tms;
    int mbps;
    int mms;
  }
}
int bload_cmd (int ac /* 0x8 */, char **av /* 0xc */)
{
  {
    int tu0;
    int ts0;
    int tut;
    int tst;
    int mu0;
    int ms0;
    int mut;
    int mst;
    int u;
    int s;
    int tb;
    int len;
    int r;
    DS_FILE *stream;
    byte *buf;
    char *path;
    word adr;
  }
}
int bsave_cmd (int ac /* 0x8 */, char **av /* 0xc */)
{
  {
    int tu0;
    int ts0;
    int tut;
    int tst;
    int mu0;
    int ms0;
    int mut;
    int mst;
    int u;
    int s;
    int tb;
    int cnt;
    int len;
    int r;
    DS_FILE *stream;
    byte *buf;
    char *path;
    word adr;
  }
}
static char TARG_IDENT[16];
int pload_cmd (int ac /* 0x8 */, char **av /* 0xc */)
{
  {
    int base;
    int id;
    int sload;
    int i;
    int r;
    byte *bp;
    byte *buf;
    DS_ELF_REGINFO *ri;
    DS_ELF_SHDR *sh;
    DS_ELF_SHDR *shdr;
    DS_ELF_PHDR *ph;
    DS_ELF_PHDR *phdr;
    DS_ELF_EHDR *ehdr;
    DS_FILE *stream;
    char *av0;
    char path[1024];
  }
}
int (*ds_rdwr_mem_align_func) (/* unknown */);
int (*ds_load_mem_func) (/* unknown */);
int (*ds_store_mem_func) (/* unknown */);
int (*ds_load_quad_reg_func) (/* unknown */);
int (*ds_store_quad_reg_func) (/* unknown */);
int (*ds_symbol_to_value_func) (/* unknown */);
int (*ds_symbol_completion_func) (/* unknown */);
int (*ds_help_completion_func) (/* unknown */);
void (*ds_idle_func) (/* unknown */);
int (*ds_check_reserved_name_func) (/* unknown */);
void (*ds_drfp_err_func) (/* unknown */);
