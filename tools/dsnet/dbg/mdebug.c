/tmp/g/tools/dsnet/dbg/mdebug.c:
typedef struct { /* size 8 */
  word sadr; /* bitsize 32, bitpos 0 */
  word eadr; /* bitsize 32, bitpos 32 */
} ADRS;
struct mdebug { /* size 88*/
  struct mdebug *forw; /* bitsize 32, bitpos 0 */
  struct mdebug *back; /* bitsize 32, bitpos 32 */
  char *path; /* bitsize 32, bitpos 64 */
  int id; /* bitsize 32, bitpos 96 */
  int base; /* bitsize 32, bitpos 128 */
  SYM_HDR *shdr; /* bitsize 32, bitpos 160 */
  byte *lins; /* bitsize 32, bitpos 192 */
  byte *line; /* bitsize 32, bitpos 224 */
  PDT *pdts; /* bitsize 32, bitpos 256 */
  PDT *pdte; /* bitsize 32, bitpos 288 */
  SYM *lsyms; /* bitsize 32, bitpos 320 */
  SYM *lsyme; /* bitsize 32, bitpos 352 */
  char *lstrs; /* bitsize 32, bitpos 384 */
  char *lstre; /* bitsize 32, bitpos 416 */
  EXT_SYM *esyms; /* bitsize 32, bitpos 448 */
  EXT_SYM *esyme; /* bitsize 32, bitpos 480 */
  char *estrs; /* bitsize 32, bitpos 512 */
  char *estre; /* bitsize 32, bitpos 544 */
  FDT *fdts; /* bitsize 32, bitpos 576 */
  FDT *fdte; /* bitsize 32, bitpos 608 */
  ADRS *fdt_adrs; /* bitsize 32, bitpos 640 */
  ADRS *pdt_adrs; /* bitsize 32, bitpos 672 */
};
typedef struct mdebug MDEBUG;
static struct { /* size 8 */
  MDEBUG *head; /* bitsize 32, bitpos 0 */
  MDEBUG *tail; /* bitsize 32, bitpos 32 */
} mdebug_list;
void clear_mdebug ()
{
  {
    MDEBUG *q;
    MDEBUG *p;
  }
}
void clear_mdebug_with_id (int id /* 0x8 */)
{
  {
    MDEBUG *q;
    MDEBUG *p;
  }
}
static void print_sym (SYM *p /* 0x8 */, char *strs /* 0xc */)
{
  {
    char *s;
  }
}
int show_mdebug (int ac /* 0x8 */, char **av /* 0xc */)
{
  {
    int nstr;
    char *str;
    EXT_SYM *esym;
    SYM *sym;
    MDEBUG *md;
  }
}
static char *set_abs_path (char *fname /* 0x8 */)
{
  {
    char *r;
    char path[1024];
  }
}
int load_mdebug (DS_FILE *stream /* 0x8 */, DS_ELF_EHDR *elf_header /* 0xc */, DS_ELF_SHDR *section_header /* 0x10 */, int mindex /* 0x14 */, int id /* 0x18 */, int base /* 0x1c */, char *path /* 0x20 */)
{
  {
    word pdt_eadr;
    word pdt_sadr;
    word fdt_eadr;
    word fdt_sadr;
    EXT_SYM *esym;
    SYM *syme;
    SYM *syms;
    SYM *sym;
    PDT *pdte;
    PDT *pdt;
    FDT *fdt;
    MDEBUG *md;
    SYM_HDR *p;
    byte *buf;
    int size;
    int offset;
  }
}
word file_and_line_to_address (int line /* 0x8 */, char *path /* 0xc */)
{
  {
    int index;
    int nfound;
    int base;
    SYM *syme;
    SYM *sym;
    ADRS *adrs;
    FDT *fdte;
    FDT *fdt;
    SYM_HDR *p;
    MDEBUG *md;
  }
}
char *address_to_file_and_line (word loc /* 0x8 */, int *pline /* 0xc */, int *f_has /* 0x10 */, int *pdelta /* 0x14 */, char **ppath /* 0x18 */)
{
  {
    word value;
    word index;
    word pdt_eadr;
    word pdt_sadr;
    int delta;
    int line;
    int nfound;
    int base;
    SYM *syme;
    SYM *sym;
    ADRS *adrs;
    PDT *pdte;
    PDT *pdt;
    FDT *fdte;
    FDT *fdt;
    SYM_HDR *p;
    MDEBUG *md;
  }
}
int symbol_to_value_by_mdebug (char *file /* 0x8 */, char *name /* 0xc */, word *pv /* 0x10 */)
{
  {
    word rpc;
    int delta;
    int index;
    int line;
    SYM *syme;
    SYM *sym;
    FDT *fdt;
    MDEBUG *md;
    {
      char _c;
    }
  }
}
static char *cur_fname;
static char *cur_buf;
static int cur_size;
static void clear_source_line_buffer ()
{
}
static char *search_source_file (char *fname /* 0x8 */, char *obj_path /* 0xc */, char *src_dirs /* 0x10 */)
{
  {
    char *q;
    char *p;
    char tmp[1024];
    char obj_dir[1024];
    static char path[1024];
  }
}
char *string_by_file_and_line (char *fname /* 0x8 */, int line /* 0xc */, char *obj_path /* 0x10 */)
{
  {
    char *r;
    char *q;
    char *pe;
    char *p;
    int i;
    DS_FILE *stream;
  }
}
int list_cmd (int ac /* 0x8 */, char **av /* 0xc */)
{
  {
    static char *next_obj_path;
    static int next_cnt;
    static int next_line;
    static char *next_file;
    int rpc_delta;
    int rpc_line;
    int delta;
    int cnt;
    int back;
    int line;
    char *obj_path;
    char *rpc_file;
    char *str;
    char *file;
    word rpc;
    word adr;
  }
}
int (*ds_rdwr_mem_align_func) (/* unknown */);
int (*ds_load_mem_func) (/* unknown */);
int (*ds_store_mem_func) (/* unknown */);
int (*ds_load_quad_reg_func) (/* unknown */);
int (*ds_store_quad_reg_func) (/* unknown */);
int (*ds_symbol_to_value_func) (/* unknown */);
int (*ds_symbol_completion_func) (/* unknown */);
int (*ds_help_completion_func) (/* unknown */);
void (*ds_idle_func) (/* unknown */);
int (*ds_check_reserved_name_func) (/* unknown */);
void (*ds_drfp_err_func) (/* unknown */);
