/tmp/g/tools/dsnet/dbg/sym.c:
struct syms { /* size 40*/
  struct syms *forw; /* bitsize 32, bitpos 0 */
  struct syms *back; /* bitsize 32, bitpos 32 */
  int id; /* bitsize 32, bitpos 64 */
  int base; /* bitsize 32, bitpos 96 */
  DS_ELF_SYMTAB *symtab; /* bitsize 32, bitpos 128 */
  char *shstrtab; /* bitsize 32, bitpos 160 */
  char *strtab; /* bitsize 32, bitpos 192 */
  int shnum; /* bitsize 32, bitpos 224 */
  int nsymtab; /* bitsize 32, bitpos 256 */
  DS_ELF_SHDR *shdr; /* bitsize 32, bitpos 288 */
};
typedef struct syms SYMS;
static struct { /* size 8 */
  SYMS *head; /* bitsize 32, bitpos 0 */
  SYMS *tail; /* bitsize 32, bitpos 32 */
} syms_list;
struct mod_syms { /* size 20*/
  struct mod_syms *forw; /* bitsize 32, bitpos 0 */
  struct mod_syms *back; /* bitsize 32, bitpos 32 */
  word adr; /* bitsize 32, bitpos 64 */
  word siz; /* bitsize 32, bitpos 96 */
  word id; /* bitsize 32, bitpos 128 */
  char name[]; /* bitpos 160 */
};
typedef struct mod_syms MOD_SYMS;
static struct { /* size 8 */
  MOD_SYMS *head; /* bitsize 32, bitpos 0 */
  MOD_SYMS *tail; /* bitsize 32, bitpos 32 */
} mod_syms_list;
void clear_module_symbol ()
{
  {
    MOD_SYMS *q;
    MOD_SYMS *p;
  }
}
void clear_module_symbol_with_name (char *name /* 0x8 */)
{
  {
    MOD_SYMS *q;
    MOD_SYMS *p;
  }
}
void add_module_symbol (char *name /* 0x8 */, int adr /* 0xc */, int siz /* 0x10 */, int id /* 0x14 */)
{
  {
    int len;
    MOD_SYMS *p;
  }
}
void clear_symbol ()
{
  {
    SYMS *q;
    SYMS *p;
  }
}
void clear_symbol_with_id (int id /* 0x8 */)
{
  {
    SYMS *q;
    SYMS *p;
  }
}
int match_symbol (char *str /* 0x8 */, int nstr /* 0xc */, char *name /* 0x10 */)
{
}
struct symline { /* size 12*/
  struct symline *forw; /* bitsize 32, bitpos 0 */
  struct symline *back; /* bitsize 32, bitpos 32 */
  word value; /* bitsize 32, bitpos 64 */
};
typedef struct symline SYMLINE;
static int add_symline (word value /* 0x8 */, char *bp /* 0xc */)
{
  {
    int n;
    SYMLINE *q;
    SYMLINE *p;
  }
}
static void show_and_free_symline ()
{
  {
    SYMLINE *q;
    SYMLINE *p;
  }
}
int module_base (int id /* 0x8 */, int base /* 0xc */, int shndx /* 0x10 */, int info /* 0x14 */)
{
}
int show_symbol (int ac /* 0x8 */, char **av /* 0xc */)
{
  {
    MOD_SYMS *mp;
    char *bp;
    char buf[1024];
    word value;
    char *str;
    int nstr;
    int i;
    DS_ELF_SYMTAB *p;
    SYMS *syms;
  }
}
int look_iopmod (DS_FILE *stream /* 0x8 */, DS_ELF_EHDR *ehdr /* 0xc */, DS_ELF_SHDR *shdr /* 0x10 */, int id /* 0x14 */, int base /* 0x18 */, void (*clear_func) (/* unknown */) /* 0x1c */)
{
  {
    int i;
    IOPMOD *iopmod;
    DS_ELF_SHDR *shiopmod;
  }
}
int look_eemod (DS_FILE *stream /* 0x8 */, DS_ELF_EHDR *ehdr /* 0xc */, DS_ELF_SHDR *shdr /* 0x10 */, int id /* 0x14 */, int base /* 0x18 */, void (*clear_func) (/* unknown */) /* 0x1c */)
{
  {
    int i;
    EEMOD *eemod;
    DS_ELF_SHDR *sheemod;
  }
}
int load_symbol (DS_FILE *stream /* 0x8 */, DS_ELF_EHDR *ehdr /* 0xc */, DS_ELF_SHDR *shdr /* 0x10 */, int symndx /* 0x14 */, int strndx /* 0x18 */, int id /* 0x1c */, int base /* 0x20 */)
{
  {
    int nsymtab;
    int n;
    int i;
    int r;
    SYMS *syms;
    char *strtab;
    char *shstrtab;
    DS_ELF_SYMTAB *psym;
    DS_ELF_SYMTAB *symtab;
    DS_ELF_SHDR *shstr;
    DS_ELF_SHDR *shsym;
  }
}
static int is_dup_sym (char *bp /* 0x8 */, char *str /* 0xc */)
{
  {
    int n;
    char *p;
    char tmp[1024];
  }
}
int address_to_module (word adr /* 0x8 */, word *padr0 /* 0xc */, word *padr1 /* 0x10 */)
{
  {
    word adr1;
    word adr0;
    MOD_SYMS *mp;
  }
}
int address_to_func (word adr /* 0x8 */, word *padr0 /* 0xc */, word *padr1 /* 0x10 */)
{
  {
    word value;
    word adr1;
    word adr0;
    int i;
    DS_ELF_SYMTAB *p;
    SYMS *syms;
  }
}
int address_to_symbol (char *buf /* 0x8 */, word adr /* 0xc */, int force_delta /* 0x10 */)
{
  {
    MOD_SYMS *mp;
    word value;
    int delta;
    int i;
    char *sp;
    char str[1024];
    char *bp;
    DS_ELF_SYMTAB *p;
    SYMS *syms;
  }
}
int symbol_to_value (char *name /* 0x8 */, word *pv /* 0xc */)
{
  {
    MOD_SYMS *mp;
    int r;
    int n;
    int i;
    char *file;
    char *cp;
    DS_ELF_SYMTAB *p;
    SYMS *syms;
    {
      char _c;
    }
  }
}
int symbol_completion (DS_HISTBUF *hb /* 0x8 */, char *name /* 0xc */)
{
  {
    MOD_SYMS *mp;
    int col;
    int lns;
    int ns;
    int nm;
    int n;
    int i;
    char *cn;
    char *p;
    DS_ELF_SYMTAB *st;
    SYMS *syms;
  }
}
int (*ds_rdwr_mem_align_func) (/* unknown */);
int (*ds_load_mem_func) (/* unknown */);
int (*ds_store_mem_func) (/* unknown */);
int (*ds_load_quad_reg_func) (/* unknown */);
int (*ds_store_quad_reg_func) (/* unknown */);
int (*ds_symbol_to_value_func) (/* unknown */);
int (*ds_symbol_completion_func) (/* unknown */);
int (*ds_help_completion_func) (/* unknown */);
void (*ds_idle_func) (/* unknown */);
int (*ds_check_reserved_name_func) (/* unknown */);
void (*ds_drfp_err_func) (/* unknown */);
static struct { /* size 8 */
  SYMLINE *head; /* bitsize 32, bitpos 0 */
  SYMLINE *tail; /* bitsize 32, bitpos 32 */
} symlines;
static int is_gcc_label (char *str /* 0x8 */)
{
  register char *str;
}
