/tmp/g/tools/dsnet/dbg/help.c:
static char *help_db[];
struct help_map { /* size 24*/
  struct help_map *forw; /* bitsize 32, bitpos 0 */
  struct help_map *back; /* bitsize 32, bitpos 32 */
  int ej; /* bitsize 32, bitpos 64 */
  int level; /* bitsize 32, bitpos 96 */
  char *key; /* bitsize 32, bitpos 128 */
  char **pl; /* bitsize 32, bitpos 160 */
};
typedef struct help_map HELP_MAP;
static struct { /* size 8 */
  HELP_MAP *head; /* bitsize 32, bitpos 0 */
  HELP_MAP *tail; /* bitsize 32, bitpos 32 */
} help_maps;
static int current_col;
static void print_char (DS_FILE *stream /* 0x8 */, int ch /* 0xc */)
{
  {
    char s[1];
  }
}
static void print_str (DS_FILE *stream /* 0x8 */, char *str /* 0xc */)
{
  {
    int n;
  }
}
static void print_nl (DS_FILE *stream /* 0x8 */)
{
  {
    char s[1];
  }
}
static void print_jis (DS_FILE *stream /* 0x8 */, char *s /* 0xc */)
{
  {
    int c1;
    int c0;
    int ki;
  }
}
static void print_sjis (DS_FILE *stream /* 0x8 */, char *s /* 0xc */)
{
  {
    int c1;
    int c0;
  }
}
static int add_help_map (int ej /* 0x8 */, int level /* 0xc */, char *key /* 0x10 */, char **pl /* 0x14 */)
{
  {
    HELP_MAP *hm;
  }
}
static void build_help_map (int lang /* 0x8 */)
{
  {
    int level;
    static int first;
    char buf[1024];
    char *q;
    char *p;
    char *ps;
    char **pl;
  }
}
int dbg_help (char *name /* 0x8 */)
{
  {
    HELP_MAP *hm;
    DS_FILE *stream;
    int n;
    int level;
    int r;
    int lang;
    char *pager;
    char *ps;
    char **pl;
  }
}
int dbg_help_completion (DS_HISTBUF *hb /* 0x8 */, char *name /* 0xc */)
{
  {
    HELP_MAP *hm;
    int lns;
    int ns;
    int nm;
    int n;
    int lang;
    char *cn;
    char *p;
  }
}
int (*ds_rdwr_mem_align_func) (/* unknown */);
int (*ds_load_mem_func) (/* unknown */);
int (*ds_store_mem_func) (/* unknown */);
int (*ds_load_quad_reg_func) (/* unknown */);
int (*ds_store_quad_reg_func) (/* unknown */);
int (*ds_symbol_to_value_func) (/* unknown */);
int (*ds_symbol_completion_func) (/* unknown */);
int (*ds_help_completion_func) (/* unknown */);
void (*ds_idle_func) (/* unknown */);
int (*ds_check_reserved_name_func) (/* unknown */);
void (*ds_drfp_err_func) (/* unknown */);
static int valid_delim (int lang /* 0x8 */, char *p /* 0xc */)
{
  register char *p;
  register int lang;
}
static int valid_lang (int lang /* 0x8 */, HELP_MAP *hm /* 0xc */)
{
  register HELP_MAP *hm;
  register int lang;
}
