/tmp/g/tools/dsnet/dbg/dbg.c:
typedef quad regtype;
//TODO: replaced for IOP: typedef unsigned int regtype;
static int f_nx;
int dsc_connected;
static int dsc_proto;
static int resetted;
static int force_reset_on_start;
static int cmd_executing;
static int expected_resetr;
static int no_kbd;
static int no_exit;
static int f_batch;
static int dsm_waiting;
static int to_sec;
static int to_usec;
static int need_getconf;
DBGP_CONF_DATA dbconf;
static int dbconf_is_valid;
static int dbconf_max_mem_align;
static int cur_state;
static int cur_proto;
static int cur_stype;
static int cur_wtype;
static void *cur_pointer;
static int cur_length;
static int cur_padding;
static int *cur_count_pointer;
static int cur_result;
static int cur_stamp;
static int cur_cpuid;
static int dcmp_waiting_status;
unsigned int dot;
unsigned int current_entry_point;
unsigned int current_gp_value;
typedef struct { /* size 16396 */
  int put; /* bitsize 32, bitpos 0 */
  int get; /* bitsize 32, bitpos 32 */
  int len; /* bitsize 32, bitpos 64 */
  char buf[16384]; /* bitsize 131072, bitpos 96 */
} TTYQ;
static TTYQ ttyq;
static int prompt_len;
static int input_line_erased;
static void ntoh_word_copy (unsigned int *dest /* 0x8 */, unsigned int *src /* 0xc */, int len /* 0x10 */)
{
  {
    int i;
  }
}
static DSP_BUF *alloc_dbgp (int id /* 0x8 */, int group /* 0xc */, int type /* 0x10 */, int code /* 0x14 */, int result /* 0x18 */, int count /* 0x1c */, void *pp /* 0x20 */, int len /* 0x24 */)
{
  {
    DBGP_HDR *ch;
    DECI2_HDR *dh;
    DSP_BUF *db;
    unsigned char count;
    unsigned char result;
    unsigned char code;
    unsigned char type;
    unsigned char group;
  }
}
static void print_prompt ()
{
  {
    char *str;
  }
}
static void erase_input_line ()
{
  {
    int n;
    int i;
    DS_HISTBUF *hb;
  }
}
static void redraw_input_line (int f_force /* 0x8 */)
{
  {
    char *p;
    DS_HISTBUF *hb;
  }
}
static void display_ttyq ()
{
  {
    int ch;
  }
}
static DSP_BUF *recv_ttyp (DS_DESC *desc /* 0x8 */, DSP_BUF *db /* 0xc */)
{
  {
    char *ep;
    char *bp;
    unsigned int *wp;
    int ch;
    int n;
    DECI2_HDR *dh;
  }
}
static void flush_tty_buf ()
{
  {
    unsigned int wv;
    DSP_BUF *db;
  }
}
static void abort_input (int code /* 0x8 */)
{
  {
    DSP_BUF *db;
    unsigned char code;
  }
}
static void normal_input (int code /* 0x8 */)
{
  {
    struct { /* size 8 */
      unsigned int zero; /* bitsize 32, bitpos 0 */
      unsigned char code; /* bitsize 8, bitpos 32 */
    } dat;
    DSP_BUF *p;
    unsigned char code;
  }
}
static DSP_BUF *recv_kbd (DS_DESC *desc /* 0x8 */, DSP_BUF *db /* 0xc */)
{
  {
    int len;
    DS_HISTBUF *hb;
    unsigned char *bp;
    unsigned char buf[1024];
    DECI2_HDR *dh;
  }
}
static void *xgkt_stream;
static void *rdimg_stream;
static int setup_xgkt (char *fname /* 0x8 */, unsigned int cnt /* 0xc */, unsigned int off /* 0x10 */)
{
}
static DSP_BUF *recv_xgkt (DSP_BUF *db /* 0x8 */)
{
  {
    int flag;
    int len;
    DBGP_XGKT_DATA *xd;
    DBGP_HDR *ch;
    DECI2_HDR *dh;
  }
}
static DSP_BUF *recv_rdimg (DSP_BUF *db /* 0x8 */)
{
  {
    int len;
    DBGP_RDIMG_DATA *rd;
    DBGP_HDR *ch;
    DECI2_HDR *dh;
  }
}
static DSP_BUF *unexpected_reply (DSP_BUF *db /* 0x8 */, DBGP_HDR *ch /* 0xc */)
{
  {
    DBGP_CONF_DATA *conf;
    int n;
    DECI2_HDR *dh;
    {
      unsigned int *wp;
    }
  }
}
static DSP_BUF *recv_dbgp (DS_DESC *desc /* 0x8 */, DSP_BUF *db /* 0xc */)
{
  {
    int i;
    int len;
    unsigned int *wq;
    unsigned int *wp;
    regtype rv;
    regtype *dp;
    regtype *sp;
    DBGP_REG *rh;
    DBGP_MEM *mh;
    DBGP_HDR *ch;
    DECI2_HDR *dh;
    {
      ILOADP_MODINFO_DATA *mi;
    }
  }
}
static void print_state_errors (char *fmt /* 0x8 */)
{
  {
    char *d;
  }
}
static int send_and_wait (DSP_BUF *db /* 0x8 */, int stype /* 0xc */, void *ptr /* 0x10 */, int len /* 0x14 */, int wresult /* 0x18 */)
{
  {
    unsigned char scode;
    unsigned char wtype;
    int sec;
    int r;
    unsigned char stype;
  }
}
static int get_conf (DBGP_CONF_DATA *conf /* 0x8 */)
{
  {
    int r;
    DBGP_CONF_DATA *p;
    DSP_BUF *db;
  }
}
static void send_dbconf ()
{
  {
    DBGP_CONF_DATA *p;
    DSP_BUF *db;
  }
}
static int rdwr_mem_align (int code /* 0x8 */, int align /* 0xc */, int cpuid /* 0x10 */, int space /* 0x14 */, unsigned int adr /* 0x18 */, void *ptr /* 0x1c */, int len /* 0x20 */)
{
  {
    int nw;
    int n;
    int amsk;
    int asiz;
    int npad;
    int nhdr;
    DBGP_MEM *mh;
    DSP_BUF *db;
    unsigned char space;
    short unsigned int cpuid;
    unsigned char align;
    unsigned char code;
  }
}
static int rdwr_mem (int code /* 0x8 */, unsigned int adr /* 0xc */, void *ptr /* 0x10 */, int len /* 0x14 */)
{
  {
    int n;
    int off;
    int amsk;
    int asiz;
    unsigned char code;
  }
}
static int load_mem (unsigned int adr /* 0x8 */, void *ptr /* 0xc */, int len /* 0x10 */)
{
}
static int store_mem (unsigned int adr /* 0x8 */, void *ptr /* 0xc */, int len /* 0x10 */)
{
}
int get_handlerlist (DBGP_HDR *phdr /* 0x8 */)
{
  {
    DSP_BUF *db;
    int id;
  }
}
int get_tcb (DBGP_HDR *phdr /* 0x8 */, int tid /* 0xc */)
{
  {
    int *ptid;
    DSP_BUF *db;
    int id;
  }
}
int get_thread_list (DBGP_HDR *phdr /* 0x8 */)
{
  {
    DSP_BUF *db;
    int id;
  }
}
int get_semablock (DBGP_HDR *phdr /* 0x8 */, int sid /* 0xc */)
{
  {
    int *psid;
    DSP_BUF *db;
    int id;
  }
}
static int load_quad_registers_id (int id /* 0x8 */, unsigned int *masks /* 0xc */, quad *pv /* 0x10 */, int n /* 0x14 */)
{
  {
    int nr;
    int k;
    int j;
    int i;
    regtype *dp;
    DBGP_REG *rh;
    DSP_BUF *db;
  }
}
static int store_quad_registers_id (int id /* 0x8 */, unsigned int *masks /* 0xc */, quad *pv /* 0x10 */, int n /* 0x14 */)
{
  {
    int nr;
    int k;
    int j;
    int i;
    regtype *dp;
    DBGP_REG *rh;
    DSP_BUF *db;
  }
}
static int access_quad_registers (int (*func) (/* unknown */) /* 0x8 */, unsigned int *masks /* 0xc */, quad *pv /* 0x10 */, int n /* 0x14 */)
{
  {
    int nvu1;
    int nvu0;
    int ncpu;
    int j;
    int i;
    unsigned int rmasks[11];
  }
}
int load_quad_registers (unsigned int *masks /* 0x8 */, quad *pv /* 0xc */, int n /* 0x10 */)
{
}
int store_quad_registers (unsigned int *masks /* 0x8 */, quad *pv /* 0xc */, int n /* 0x10 */)
{
}
int load_word_registers (unsigned int *masks /* 0x8 */, unsigned int *pv /* 0xc */, int n /* 0x10 */)
{
  {
    int nr;
    int k;
    int j;
    int i;
    regtype *dp;
    DBGP_REG *rh;
    DSP_BUF *db;
    int id;
  }
}
int store_word_registers (unsigned int *masks /* 0x8 */, unsigned int *pv /* 0xc */, int n /* 0x10 */)
{
  {
    int nr;
    int k;
    int j;
    int i;
    regtype *dp;
    DBGP_REG *rh;
    DSP_BUF *db;
    int id;
  }
}
int cont_and_wait_halt (int code /* 0x8 */, int cnt /* 0xc */)
{
  {
    DSP_BUF *db;
    unsigned char cnt;
    unsigned char code;
  }
}
int run_and_wait_halt (unsigned int entry_point /* 0x8 */, unsigned int gp_value /* 0xc */, int argc /* 0x10 */, char *args /* 0x14 */, int args_len /* 0x18 */)
{
  {
    DBGP_EERUN *er;
    DSP_BUF *db;
  }
}
int break_and_wait_halt ()
{
  {
    DSP_BUF *db;
  }
}
int wait_halt ()
{
}
int get_brkpt (DBGP_BRKPT_DATA *bps /* 0x8 */, int *pn /* 0xc */)
{
  {
    DSP_BUF *db;
  }
}
int put_brkpt (DBGP_BRKPT_DATA *bps /* 0x8 */, int n /* 0xc */)
{
  {
    int i;
    DBGP_BRKPT_DATA *dp;
    DSP_BUF *db;
  }
}
int is_target_is_running ()
{
}
static int dsip_stamp;
static DSP_BUF *recv_loadp (DS_DESC *desc /* 0x8 */, DSP_BUF *db /* 0xc */)
{
  {
    int i;
    ILOADP_MODINFO_DATA *mi;
    unsigned int *wpe;
    unsigned int *wp;
    unsigned int id;
    int r;
    int len;
    ILOADP_HDR *ip;
    DECI2_HDR *dh;
    {
      unsigned int vals[2];
    }
  }
}
int send_iload_and_wait (int cmd /* 0x8 */, int action /* 0xc */, unsigned int id /* 0x10 */, void *ptr /* 0x14 */, int len /* 0x18 */, int nodisp /* 0x1c */)
{
  {
    unsigned int *wp;
    int n;
    int r;
    ILOADP_HDR *ip;
    DECI2_HDR *dh;
    DSP_BUF *db2;
    DSP_BUF *db;
    unsigned char action;
    unsigned char cmd;
  }
}
static int get_and_check_config ()
{
  {
    int n;
  }
}
static void print_size (unsigned int size /* 0x8 */)
{
  {
    int following;
  }
}
static void print_align_list (unsigned int align /* 0x8 */)
{
  {
    int following;
    int i;
  }
}
static int show_dbconf (int ac /* 0x8 */, char **av /* 0xc */)
{
  {
    DBGP_CONF_DATA *p;
  }
}
int is_target_exec_ctl ()
{
}
int is_automatic_prefix_breakpoint ()
{
}
int is_describe_ub_all ()
{
}
int is_lstep_stop_at_no_line ()
{
}
void di_format (int *f_adr /* 0x8 */, int *f_iw /* 0xc */, int *f_ba /* 0x10 */, int *f_ma /* 0x14 */)
{
}
char *dr_format_str ()
{
}
char *source_directories_str ()
{
}
int get_help_lang ()
{
  {
    char *lang;
  }
}
char *get_help_pager ()
{
}
static int reset_cmd (int ac /* 0x8 */, char **av /* 0xc */)
{
  {
    unsigned int wv;
    quad qv;
    char *istr;
    char *estr;
    NETMP_BOOTP mb;
    int ccon;
    int crst;
  }
}
static int dbgctl (int id /* 0x8 */, int flag /* 0xc */)
{
  {
    unsigned int *wp;
    DSP_BUF *db;
  }
}
static int dbgctl_cmd (int ac /* 0x8 */, char **av /* 0xc */)
{
  {
    int flag;
    int id;
  }
}
static int xgkt_cmd (int ac /* 0x8 */, char **av /* 0xc */)
{
  {
    DBGP_XGKT_CTL *xc;
    DSP_BUF *db;
    unsigned int off;
    unsigned int cnt;
    char *fname;
  }
}
static int storeimage_cmd (int ac /* 0x8 */, char **av /* 0xc */)
{
  {
    DBGP_RDIMG *ri;
    DSP_BUF *db;
    int h;
    int w;
    int y;
    int x;
    int spsm;
    int sbw;
    int sbp;
    int r;
    char *fname;
  }
}
static int bpfunc_cmd (int ac /* 0x8 */, char **av /* 0xc */)
{
  {
    DSP_BUF *db;
    unsigned int *wp;
    unsigned int adr;
  }
}
static int argc;
static char **argv;
static DSP_BUF *recv_dcmp (DS_DESC *desc /* 0x8 */, DSP_BUF *db /* 0xc */)
{
  {
    DCMP_HDR *ch;
    DECI2_HDR *dh;
    {
      DCMP_STATUS_DATA *sd;
    }
    {
      DECI2_HDR *odh;
      DCMP_ERROR_DATA *ed;
    }
  }
}
static void batch (int ac /* 0x8 */, char **av /* 0xc */)
{
  {
    int r;
    char *q;
    char *p;
    char buf[1024];
  }
}
static DSP_BUF *recv_netmp (DS_DESC *desc /* 0x8 */, DSP_BUF *db /* 0xc */)
{
  {
    NETMP_HDR *mh;
    DECI2_HDR *dh;
    {
      int n;
    }
  }
}
static int send_netmp_connect_request ()
{
  {
    int i;
    int pri;
    NETMP_PROTOS *p;
    NETMP_PROTOS protos[26];
    {
      NETMP_PROTOS *_p;
    }
    {
      NETMP_PROTOS *_p;
    }
    {
      NETMP_PROTOS *_p;
    }
    {
      NETMP_PROTOS *_p;
    }
    {
      NETMP_PROTOS *_p;
    }
    {
      NETMP_PROTOS *_p;
    }
    {
      NETMP_PROTOS *_p;
    }
    {
      NETMP_PROTOS *_p;
    }
  }
}
static DSP_BUF *recv_console (DS_DESC *desc /* 0x8 */, DSP_BUF *db /* 0xc */)
{
  {
    int len;
    DECI2_HDR *dh;
  }
}
static int start_console (DS_DESC *desc /* 0x8 */)
{
}
static void drfp_error (DSP_BUF *db /* 0x8 */)
{
}
static int usage (int f_true /* 0x8 */)
{
}
static void dup_and_exe (char *cmd /* 0x8 */)
{
  {
    char *buf;
  }
}
void dr_default_di ()
{
}
void ex_default_dr ()
{
}
void dr0_default_di ()
{
}
void ex0_default_dr ()
{
}
void dr1_default_di ()
{
}
void ex1_default_dr ()
{
}
void lstep_default_list ()
{
}
static void set_options_to_default ()
{
  {
    char *langenv;
    char *ibootp_val;
    char *ebootp_val;
    char *target_name;
  }
}
int main (int ac /* 0x8 */, char **av /* 0xc */)
{
  {
    char *port_name;
    int i;
    int r;
  }
}
int IsSupported (int MajorVersion /* 0x8 */, int MinorVersion /* 0xc */)
{
}
int (*ds_rdwr_mem_align_func) (/* unknown */);
int (*ds_load_mem_func) (/* unknown */);
int (*ds_store_mem_func) (/* unknown */);
int (*ds_load_quad_reg_func) (/* unknown */);
int (*ds_store_quad_reg_func) (/* unknown */);
int (*ds_symbol_to_value_func) (/* unknown */);
int (*ds_symbol_completion_func) (/* unknown */);
int (*ds_help_completion_func) (/* unknown */);
void (*ds_idle_func) (/* unknown */);
int (*ds_check_reserved_name_func) (/* unknown */);
void (*ds_drfp_err_func) (/* unknown */);
unsigned int regbuf_mask[11];
//TODO: Repalced for IOP: unsigned int regbuf_mask[10];
struct regbuf_vals[11][32];
//TODO: Replaced for IOP: unsigned int regbuf_vals[10][32];
static DS_OPTION *opt_target_name;
static DS_OPTION *opt_tty_mask;
static DS_OPTION *opt_atty_mask;
static DS_OPTION *opt_file_priority;
static DS_OPTION *opt_reset_on_start;
static DS_OPTION *opt_check_manager_version;
static DS_OPTION *opt_dr_default_format;
static DS_OPTION *opt_dr_default_di;
static DS_OPTION *opt_ex_default_dr;
static DS_OPTION *opt_dr0_default_di;
static DS_OPTION *opt_ex0_default_dr;
static DS_OPTION *opt_dr1_default_di;
static DS_OPTION *opt_ex1_default_dr;
static DS_OPTION *opt_lstep_default_list;
static DS_OPTION *opt_lstep_stop_at_no_line;
static DS_OPTION *opt_source_directories;
static DS_OPTION *opt_target_exec_ctl_config;
static DS_OPTION *opt_target_exec_ctl_override;
static DS_OPTION *opt_initial_ebootp;
static DS_OPTION *opt_current_ebootp;
static DS_OPTION *opt_initial_ibootp;
static DS_OPTION *opt_current_ibootp;
static DS_OPTION *opt_automatic_prefix_breakpoint;
static DS_OPTION *opt_describe_ub_all;
static DS_OPTION *opt_xgkt_flag;
static DS_OPTION *opt_di_address;
static DS_OPTION *opt_di_instruction_word;
static DS_OPTION *opt_di_branch_address;
static DS_OPTION *opt_di_macro;
static DS_OPTION *opt_help_lang;
static DS_OPTION *opt_help_pager;
static DS_OPTION *opt_iopconf;
static DS_OPTION *opt_iopmodules;
static DS_DESC *target_desc;
static DS_DESC *kbd_desc;
static int xgkt_off;
static int xgkt_eoff;
static int xgkt_seq;
static int xgkt_glen;
static int xgkt_vbs0;
static int xgkt_vif1;
static int rdimg_seq;
static int rdimg_len;
