/tmp/g/tools/dsnet/dbg/ireg.c:
typedef struct { /* size 12 */
  int off; /* bitsize 32, bitpos 0 */
  int bit; /* bitsize 32, bitpos 32 */
  char *name; /* bitsize 32, bitpos 64 */
} IOP_REGS;
static IOP_REGS iop_regs[];
static void print_symcr (word rcr /* 0x8 */)
{
}
static void print_rcr (word rcr /* 0x8 */)
{
}
static void print_symsr (word rsr /* 0x8 */)
{
}
static void print_rsr (word rsr /* 0x8 */)
{
}
static void print_dcic (word dcic /* 0x8 */)
{
}
static int set_regmasks (word *masks /* 0x8 */, char *name /* 0xc */)
{
  {
    int rf;
    int n;
    IOP_REGS *reg;
    char *p;
    {
      char _c;
    }
  }
}
int check_reserved_name (char *name /* 0x8 */)
{
  {
    IOP_REGS *reg;
  }
}
int load_word_reg (char *name /* 0x8 */, word *pv /* 0xc */)
{
  {
    int j;
    int i;
    word vals[2];
    word masks[10];
  }
}
int store_word_reg (char *name /* 0x8 */, word val /* 0xc */)
{
  {
    int n;
    int i;
    word vals[320];
    word masks[10];
  }
}
int load_quad_reg (char *name /* 0x8 */, quad *pv /* 0xc */)
{
  {
    word wv;
  }
}
int store_quad_reg (char *name /* 0x8 */, quad val /* 0xc */)
{
}
static int dr_default_setmask (word *masks /* 0x8 */, int off /* 0xc */, int bit /* 0x10 */)
{
}
static int dr_default (char *fmt /* 0x8 */, word *masks /* 0xc */, word *vals /* 0x10 */)
{
  {
    int ch;
    int nreg;
    int n;
    IOP_REGS *reg;
    char *p;
    {
      char _c;
    }
    {
      char _c;
    }
    {
      char _c;
    }
    {
      word rpc;
      word repc;
      word rcr;
    }
    {
      char _c;
    }
    {
      char _c;
    }
    {
      char _c;
    }
  }
}
int dreg_cmd (int ac /* 0x8 */, char **av /* 0xc */)
{
  {
    IOP_REGS *reg;
    int last_off;
    int i;
    int n;
    int r;
    word *pv;
    word vals[320];
    word masks[10];
    {
      char *fmt;
      word rpc;
      word repc;
      word rcr;
      word rsr;
      word rbada;
      word *gpr;
      word *hilo;
    }
  }
}
int sreg_cmd (int ac /* 0x8 */, char **av /* 0xc */)
{
  {
    int f_force;
    word wv;
  }
}
int hbp_cmd (int ac /* 0x8 */, char **av /* 0xc */)
{
  {
    int f_debug;
    int hub;
    word msk;
    word adr;
    char *q;
    char *p;
    char msks[1024];
    char adrs[1024];
    char type[1024];
    word regs[5];
    word masks[10];
  }
}
int rload_cmd (int ac /* 0x8 */, char **av /* 0xc */)
{
  {
    int n;
    int siz;
    int r;
    IOP_REGS *reg;
    word val;
    word tvals[320];
    word avals[320];
    word tmasks[10];
    word amasks[10];
    DS_FILE *stream;
    char vstr[1024];
    char rstr[1024];
    char *q;
    char *pe;
    char *p;
    char *buf;
    char *fname;
    {
      char _c;
    }
  }
}
int rsave_cmd (int ac /* 0x8 */, char **av /* 0xc */)
{
  {
    int n;
    int r;
    IOP_REGS *reg;
    word vals[320];
    word masks[10];
    DS_FILE *stream;
    char buf[1024];
    char *fname;
  }
}
int (*ds_rdwr_mem_align_func) (/* unknown */);
int (*ds_load_mem_func) (/* unknown */);
int (*ds_store_mem_func) (/* unknown */);
int (*ds_load_quad_reg_func) (/* unknown */);
int (*ds_store_quad_reg_func) (/* unknown */);
int (*ds_symbol_to_value_func) (/* unknown */);
int (*ds_symbol_completion_func) (/* unknown */);
int (*ds_help_completion_func) (/* unknown */);
void (*ds_idle_func) (/* unknown */);
int (*ds_check_reserved_name_func) (/* unknown */);
void (*ds_drfp_err_func) (/* unknown */);
