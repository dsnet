 mdebug.h:
typedef struct { /* size 96 */
  half magic; /* bitsize 16, bitpos 0 */
  half vstamp; /* bitsize 16, bitpos 16 */
  word ilineMax; /* bitsize 32, bitpos 32 */
  word cbLine; /* bitsize 32, bitpos 64 */
  word cblineOffset; /* bitsize 32, bitpos 96 */
  word idnMax; /* bitsize 32, bitpos 128 */
  word cbDnOffset; /* bitsize 32, bitpos 160 */
  word ipdMax; /* bitsize 32, bitpos 192 */
  word cbPdOffset; /* bitsize 32, bitpos 224 */
  word isymMax; /* bitsize 32, bitpos 256 */
  word cbSymOffset; /* bitsize 32, bitpos 288 */
  word ioptMax; /* bitsize 32, bitpos 320 */
  word cbOptOffset; /* bitsize 32, bitpos 352 */
  word iauxMax; /* bitsize 32, bitpos 384 */
  word cbAuxOffset; /* bitsize 32, bitpos 416 */
  word issMax; /* bitsize 32, bitpos 448 */
  word cbSsOffset; /* bitsize 32, bitpos 480 */
  word issExtMax; /* bitsize 32, bitpos 512 */
  word cbSsExtOffset; /* bitsize 32, bitpos 544 */
  word ifdMax; /* bitsize 32, bitpos 576 */
  word cbFdOffset; /* bitsize 32, bitpos 608 */
  word crfd; /* bitsize 32, bitpos 640 */
  word cbRfdOffset; /* bitsize 32, bitpos 672 */
  word iextMax; /* bitsize 32, bitpos 704 */
  word cbExtOffset; /* bitsize 32, bitpos 736 */
} SYM_HDR;
typedef struct { /* size 12 */
  word iss; /* bitsize 32, bitpos 0 */
  word value; /* bitsize 32, bitpos 32 */
  word st_sc_index; /* bitsize 32, bitpos 64 */
} SYM;
typedef struct { /* size 16 */
  half reserved; /* bitsize 16, bitpos 0 */
  half ifd; /* bitsize 16, bitpos 16 */
  SYM sym; /* bitsize 96, bitpos 32 */
} EXT_SYM;
typedef struct { /* size 72 */
  word adr; /* bitsize 32, bitpos 0 */
  word rss; /* bitsize 32, bitpos 32 */
  word issBase; /* bitsize 32, bitpos 64 */
  word cbSs; /* bitsize 32, bitpos 96 */
  word isymBase; /* bitsize 32, bitpos 128 */
  word csym; /* bitsize 32, bitpos 160 */
  word ilineBase; /* bitsize 32, bitpos 192 */
  word cline; /* bitsize 32, bitpos 224 */
  word ioptBase; /* bitsize 32, bitpos 256 */
  word copt; /* bitsize 32, bitpos 288 */
  half ipdFirst; /* bitsize 16, bitpos 320 */
  half cpd; /* bitsize 16, bitpos 336 */
  word iauxBase; /* bitsize 32, bitpos 352 */
  word caux; /* bitsize 32, bitpos 384 */
  word rfdBase; /* bitsize 32, bitpos 416 */
  word crfd; /* bitsize 32, bitpos 448 */
  word misc; /* bitsize 32, bitpos 480 */
  word cbLineOffset; /* bitsize 32, bitpos 512 */
  word cbLine; /* bitsize 32, bitpos 544 */
} FDT;
typedef struct { /* size 52 */
  word adr; /* bitsize 32, bitpos 0 */
  word isym; /* bitsize 32, bitpos 32 */
  word iline; /* bitsize 32, bitpos 64 */
  word regmask; /* bitsize 32, bitpos 96 */
  word regoffset; /* bitsize 32, bitpos 128 */
  word iopt; /* bitsize 32, bitpos 160 */
  word fregmask; /* bitsize 32, bitpos 192 */
  word fregoffset; /* bitsize 32, bitpos 224 */
  word frameoffset; /* bitsize 32, bitpos 256 */
  half framereg; /* bitsize 16, bitpos 288 */
  half pcreg; /* bitsize 16, bitpos 304 */
  word lnLow; /* bitsize 32, bitpos 320 */
  word lnHigh; /* bitsize 32, bitpos 352 */
  word cbLineOffset; /* bitsize 32, bitpos 384 */
} PDT;
