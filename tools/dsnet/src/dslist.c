/tmp/g/tools/dsnet/src/dslist.c:
static int f_verbose;
static int f_long;
static int f_ncmv;
static int stamp;
static int dsm_waiting;
static int to_sec;
static int to_usec;
static int waiting_listr;
static void send_xload (int cmd /* 0x8 */, int action /* 0xc */, int id /* 0x10 */, void *ptr /* 0x14 */, int len /* 0x18 */)
{
  {
    ELOADP_HDR *lp;
    DECI2_HDR *dh;
    DSP_BUF *db;
  }
}
static void show_info (ELOADP_HDR *lp /* 0x8 */)
{
  {
    unsigned int gp_value;
    unsigned int entry_address;
    unsigned int bsize;
    unsigned int dsize;
    unsigned int tsize;
    unsigned int mod_addr;
    short unsigned int flags;
    short unsigned int vers;
    unsigned int id;
    ELOADP_MODINFO_DATA *info;
    {
      unsigned int lib_size;
      unsigned int lib_addr;
      unsigned int stub_size;
      unsigned int stub_addr;
    }
  }
}
static DSP_BUF *recv_xloadp (DS_DESC *desc /* 0x8 */, DSP_BUF *db /* 0xc */)
{
  {
    static unsigned int *ide;
    static unsigned int *ids;
    int len;
    int r;
    unsigned int id;
    ELOADP_HDR *lp;
    DECI2_HDR *dh;
  }
}
static DSP_BUF *recv_dcmp (DS_DESC *desc /* 0x8 */, DSP_BUF *db /* 0xc */)
{
  {
    DCMP_HDR *ch;
    DECI2_HDR *dh;
    {
      DECI2_HDR *orig_dh;
      DCMP_ERROR_DATA *ed;
    }
    {
      DCMP_STATUS_DATA *sd;
    }
  }
}
static DSP_BUF *recv_netmp (DS_DESC *desc /* 0x8 */, DSP_BUF *db /* 0xc */)
{
  {
    NETMP_HDR *mh;
    DECI2_HDR *dh;
    {
      int n;
    }
  }
}
static int usage (int f_true /* 0x8 */)
{
}
int main (int ac /* 0x8 */, char **av /* 0xc */)
{
  {
    int r;
    NETMP_PROTOS protos;
    char *target_name;
  }
}
int (*ds_rdwr_mem_align_func) (/* unknown */);
int (*ds_load_mem_func) (/* unknown */);
int (*ds_store_mem_func) (/* unknown */);
int (*ds_load_quad_reg_func) (/* unknown */);
int (*ds_store_quad_reg_func) (/* unknown */);
int (*ds_symbol_to_value_func) (/* unknown */);
int (*ds_symbol_completion_func) (/* unknown */);
int (*ds_help_completion_func) (/* unknown */);
void (*ds_idle_func) (/* unknown */);
int (*ds_check_reserved_name_func) (/* unknown */);
void (*ds_drfp_err_func) (/* unknown */);
static DS_DESC *target_desc;
