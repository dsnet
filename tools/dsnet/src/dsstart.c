/tmp/g/tools/dsnet/src/dsstart.c:
static int f_verbose;
static int f_ncmv;
static int f_nttyp;
static int f_nostty;
static int f_ndrfp;
static int f_wait;
static int stamp;
static int id;
static int dsm_waiting;
static int to_sec;
static int to_usec;
static int waiting_startr;
static int waiting_status;
static int proto_drfp;
static void drfp_error (DSP_BUF *db /* 0x8 */)
{
}
static void send_xstart ()
{
  {
    ELOADP_HDR *lp;
    DECI2_HDR *dh;
    DSP_BUF *db;
  }
}
static DSP_BUF *recv_xloadp (DS_DESC *desc /* 0x8 */, DSP_BUF *db /* 0xc */)
{
  {
    int r;
    ELOADP_HDR *lp;
    DECI2_HDR *dh;
  }
}
static DSP_BUF *recv_dstp (DS_DESC *desc /* 0x8 */, DSP_BUF *db /* 0xc */)
{
  {
    int len;
    int i;
    unsigned char *bp;
    DECI2_HDR *dh;
  }
}
static DSP_BUF *recv_dcmp (DS_DESC *desc /* 0x8 */, DSP_BUF *db /* 0xc */)
{
  {
    DCMP_HDR *ch;
    DECI2_HDR *dh;
    {
      ELOADP_HDR *lp;
      DECI2_HDR *orig_dh;
      DCMP_ERROR_DATA *ed;
    }
    {
      int last_status;
      DCMP_STATUS_DATA *sd;
    }
  }
}
static DSP_BUF *recv_netmp (DS_DESC *desc /* 0x8 */, DSP_BUF *db /* 0xc */)
{
  {
    NETMP_HDR *mh;
    DECI2_HDR *dh;
    {
      int n;
    }
  }
}
static void set_proto (char *prog /* 0x8 */)
{
  {
    char c;
  }
}
static void set_args (int ac /* 0x8 */, char **av /* 0xc */)
{
  {
    int i;
    int f_dev;
    char *p;
  }
}
static int usage (int f_true /* 0x8 */)
{
}
int main (int ac /* 0x8 */, char **av /* 0xc */)
{
  {
    int i;
    int r;
    NETMP_PROTOS *p;
    NETMP_PROTOS protos[14];
    char *iopmodules;
    char *iopconf;
    char *target_name;
    {
      NETMP_PROTOS *_p;
    }
    {
      NETMP_PROTOS *_p;
    }
    {
      NETMP_PROTOS *_p;
    }
    {
      NETMP_PROTOS *_p;
    }
    {
      NETMP_PROTOS *_p;
    }
  }
}
int (*ds_rdwr_mem_align_func) (/* unknown */);
int (*ds_load_mem_func) (/* unknown */);
int (*ds_store_mem_func) (/* unknown */);
int (*ds_load_quad_reg_func) (/* unknown */);
int (*ds_store_quad_reg_func) (/* unknown */);
int (*ds_symbol_to_value_func) (/* unknown */);
int (*ds_symbol_completion_func) (/* unknown */);
int (*ds_help_completion_func) (/* unknown */);
void (*ds_idle_func) (/* unknown */);
int (*ds_check_reserved_name_func) (/* unknown */);
void (*ds_drfp_err_func) (/* unknown */);
static DS_DESC *target_desc;
static int arg_siz;
static char args[160];
static DS_OPTION *opt_iopconf;
static DS_OPTION *opt_iopmodules;
