/tmp/g/tools/dsnet/lib/drfp.c:
struct handle { /* size 16*/
  int fd; /* bitsize 32, bitpos 0 */
  DIR *dir; /* bitsize 32, bitpos 32 */
  int ino; /* bitsize 32, bitpos 64 */
  char *name; /* bitsize 32, bitpos 96 */
};
int ds_drfp_opened_files;
void (*ds_drfp_err_func) (/* unknown */);
static DS_DESC *rex_desc;
static DSP_BUF *rex_buf;
static int rex_buf_status;
static void replace_rex_buf (DS_DESC *desc /* 0x8 */, DSP_BUF *db /* 0xc */)
{
}
DSP_BUF *ds_recv_drfp_dcmp (DS_DESC *desc /* 0x8 */, DSP_BUF *db /* 0xc */)
{
  {
    byte *q;
    byte *p;
    int len;
    DSP_BUF *db2;
    DCMP_HDR *ch;
    DECI2_HDR *dh;
  }
}
static void drfp_errmsg (int proto /* 0x8 */, char *tag /* 0xc */, char *fname /* 0x10 */, char *err /* 0x14 */)
{
  {
    DSP_BUF *db;
    int len;
    char *p;
    char *msg;
    char *prog;
    char path[4096];
  }
}
static void init_handle_map ()
{
  {
    int i;
    static int once;
  }
}
static int alloc_handle ()
{
  {
    int i;
  }
}
static void free_handle (int handle /* 0x8 */)
{
}
static int get_ino (char *name /* 0x8 */)
{
  {
    struct stat64 stat;
  }
}
static int is_found (char *name /* 0x8 */)
{
  {
    struct stat64 stat;
  }
}
static int is_dir (char *name /* 0x8 */)
{
  {
    struct stat64 stat;
  }
}
static int is_file (char *name /* 0x8 */)
{
  {
    struct stat64 stat;
  }
}
static int is_opened_ino (int ino /* 0x8 */)
{
  {
    int i;
  }
}
static int handle_to_fd (int handle /* 0x8 */)
{
}
static DIR *handle_to_dir (int handle /* 0x8 */)
{
}
static int drfp_recv_reset ()
{
  {
    int i;
    int r;
  }
}
static DSP_BUF *send_drfp (DS_DESC *desc /* 0x8 */, DSP_BUF *rdb /* 0xc */, DECI2_HDR *rdh /* 0x10 */, word code /* 0x14 */, word seq /* 0x18 */, word result /* 0x1c */, void *data /* 0x20 */, word addlen /* 0x24 */)
{
  {
    DSP_BUF *db;
    word *args;
    int n;
  }
}
static word drfp_flag (word flag /* 0x8 */, word *pmode /* 0xc */)
{
  {
    word r;
  }
}
static word drfp_err (word err /* 0x8 */)
{
  {
    word r;
  }
}
static word drfp_base (word base /* 0x8 */)
{
}
static void _ds_option_expand (char *path /* 0x8 */, char *name /* 0xc */)
{
  {
    int n;
    char *p;
    char buf[4096];
  }
}
static DSP_BUF *recv_drfp_open (DS_DESC *desc /* 0x8 */, DSP_BUF *db /* 0xc */, DECI2_HDR *dh /* 0x10 */, word *args /* 0x14 */, int arglen /* 0x18 */)
{
  {
    struct stat64 stbuf;
    char path[4096];
    int handle;
    int r;
    char *name;
    word fd;
    word result;
    word mode;
    word flag;
    word seq;
  }
}
static DSP_BUF *recv_drfp_dopen (DS_DESC *desc /* 0x8 */, DSP_BUF *db /* 0xc */, DECI2_HDR *dh /* 0x10 */, word *args /* 0x14 */, int arglen /* 0x18 */)
{
  {
    char path[4096];
    DIR *dirstream;
    int handle;
    char *name;
    word fd;
    word result;
    word seq;
  }
}
static void set_scestat_time (byte *st_time /* 0x8 */, time_t *time /* 0xc */)
{
  {
    struct tm *tm;
  }
}
static void set_stat_time (time_t *time /* 0x8 */, byte *st_time /* 0xc */)
{
  {
    struct tm tm;
  }
}
static word sce_mode2st_mode (word sce_mode /* 0x8 */)
{
  {
    word st_mode;
  }
}
static word st_mode2sce_mode (word st_mode /* 0x8 */)
{
  {
    word sce_mode;
  }
}
static void sce_stat2stat64 (struct stat64 *to /* 0x8 */, struct sce_stat *from /* 0xc */)
{
  {
    word mode;
    word st_hisize;
  }
}
static void stat642sce_stat (struct sce_stat *to /* 0x8 */, struct stat64 *from /* 0xc */)
{
}
static int getsce_stat64 (char *name /* 0x8 */, struct sce_stat *data /* 0xc */)
{
  {
    int r;
    word result;
    struct stat64 stbuf;
  }
}
static DSP_BUF *recv_drfp_getstat (DS_DESC *desc /* 0x8 */, DSP_BUF *db /* 0xc */, DECI2_HDR *dh /* 0x10 */, word *args /* 0x14 */, int arglen /* 0x18 */)
{
  {
    struct sce_stat data;
    char path[4096];
    int r;
    char *name;
    word len;
    word result;
    word seq;
  }
}
static DSP_BUF *recv_drfp_remove (DS_DESC *desc /* 0x8 */, DSP_BUF *db /* 0xc */, DECI2_HDR *dh /* 0x10 */, word *args /* 0x14 */, int arglen /* 0x18 */)
{
  {
    char path[4096];
    int r;
    char *name;
    word result;
    word seq;
  }
}
static DSP_BUF *recv_drfp_rename (DS_DESC *desc /* 0x8 */, DSP_BUF *db /* 0xc */, DECI2_HDR *dh /* 0x10 */, word *args /* 0x14 */, int arglen /* 0x18 */)
{
  {
    char newpath[4096];
    char oldpath[4096];
    int r;
    char *newname;
    char *oldname;
    word result;
    word seq;
  }
}
static DSP_BUF *recv_drfp_chdir (DS_DESC *desc /* 0x8 */, DSP_BUF *db /* 0xc */, DECI2_HDR *dh /* 0x10 */, word *args /* 0x14 */, int arglen /* 0x18 */)
{
  {
    char *name;
    word result;
    word seq;
  }
}
static DSP_BUF *recv_drfp_rmdir (DS_DESC *desc /* 0x8 */, DSP_BUF *db /* 0xc */, DECI2_HDR *dh /* 0x10 */, word *args /* 0x14 */, int arglen /* 0x18 */)
{
  {
    char path[4096];
    int r;
    char *name;
    word result;
    word seq;
  }
}
static DSP_BUF *recv_drfp_mkdir (DS_DESC *desc /* 0x8 */, DSP_BUF *db /* 0xc */, DECI2_HDR *dh /* 0x10 */, word *args /* 0x14 */, int arglen /* 0x18 */)
{
  {
    char path[4096];
    int r;
    char *name;
    word result;
    word mode;
    word seq;
  }
}
static DSP_BUF *recv_drfp_dclose (DS_DESC *desc /* 0x8 */, DSP_BUF *db /* 0xc */, DECI2_HDR *dh /* 0x10 */, word *args /* 0x14 */, int arglen /* 0x18 */)
{
  {
    DIR *dirent;
    int r;
    int handle;
    word result;
    word seq;
  }
}
static DSP_BUF *recv_drfp_close (DS_DESC *desc /* 0x8 */, DSP_BUF *db /* 0xc */, DECI2_HDR *dh /* 0x10 */, word *args /* 0x14 */, int arglen /* 0x18 */)
{
  {
    int fd;
    int handle;
    word result;
    word seq;
  }
}
static DSP_BUF *recv_drfp_read (DS_DESC *desc /* 0x8 */, DSP_BUF *db /* 0xc */, DECI2_HDR *dh /* 0x10 */, word *args /* 0x14 */, int arglen /* 0x18 */)
{
  {
    int fd;
    int handle;
    int r;
    byte *data;
    word *plen;
    word result;
    word len;
    word seq;
  }
}
static DSP_BUF *recv_drfp_write (DS_DESC *desc /* 0x8 */, DSP_BUF *db /* 0xc */, DECI2_HDR *dh /* 0x10 */, word *args /* 0x14 */, int arglen /* 0x18 */)
{
  {
    int fd;
    int handle;
    int r;
    byte *data;
    word result;
    word len;
    word seq;
  }
}
static DSP_BUF *recv_drfp_seek (DS_DESC *desc /* 0x8 */, DSP_BUF *db /* 0xc */, DECI2_HDR *dh /* 0x10 */, word *args /* 0x14 */, int arglen /* 0x18 */)
{
  {
    int fd;
    int handle;
    int base;
    int off;
    word pos;
    word result;
    word seq;
    {
      long long int curpos;
      long long int newpos;
    }
  }
}
static DSP_BUF *recv_drfp_seek64 (DS_DESC *desc /* 0x8 */, DSP_BUF *db /* 0xc */, DECI2_HDR *dh /* 0x10 */, word *args /* 0x14 */, int arglen /* 0x18 */)
{
  {
    word pos[2];
    word result;
    int base;
    int fd;
    int handle;
    word lopos;
    word hipos;
    word hioff;
    word looff;
    word seq;
    {
      long long int off;
      long long int newpos;
    }
  }
}
static DSP_BUF *recv_drfp_dread (DS_DESC *desc /* 0x8 */, DSP_BUF *db /* 0xc */, DECI2_HDR *dh /* 0x10 */, word *args /* 0x14 */, int arglen /* 0x18 */)
{
  {
    DIR *dirstream;
    struct sce_dirent *sce_dent;
    struct dirent64 *dent;
    int r;
    word handle;
    word result;
    word len;
    word seq;
    {
      char *filename;
    }
  }
}
static DSP_BUF *recv_drfp_symlink (DS_DESC *desc /* 0x8 */, DSP_BUF *db /* 0xc */, DECI2_HDR *dh /* 0x10 */, word *args /* 0x14 */, int arglen /* 0x18 */)
{
  {
    word result;
    word seq;
    char newpath[4096];
    char existpath[4096];
    char *newname;
    char *existname;
  }
}
static DSP_BUF *recv_drfp_readlink (DS_DESC *desc /* 0x8 */, DSP_BUF *db /* 0xc */, DECI2_HDR *dh /* 0x10 */, word *args /* 0x14 */, int arglen /* 0x18 */)
{
  {
    word len;
    word result;
    word seq;
    char newpath[4096];
    char existpath[4096];
    char *newname;
    char *existname;
  }
}
static DSP_BUF *recv_drfp_chstat (DS_DESC *desc /* 0x8 */, DSP_BUF *db /* 0xc */, DECI2_HDR *dh /* 0x10 */, word *args /* 0x14 */, int arglen /* 0x18 */)
{
  {
    struct sce_stat *pdata;
    struct stat64 data;
    char path[4096];
    word field;
    char *name;
    word result;
    word seq;
  }
}
DSP_BUF *ds_recv_drfp (DS_DESC *desc /* 0x8 */, DSP_BUF *db /* 0xc */)
{
  {
    word *args;
    int len;
    DECI2_HDR *dh;
  }
}
int (*ds_rdwr_mem_align_func) (/* unknown */);
int (*ds_load_mem_func) (/* unknown */);
int (*ds_store_mem_func) (/* unknown */);
int (*ds_load_quad_reg_func) (/* unknown */);
int (*ds_store_quad_reg_func) (/* unknown */);
int (*ds_symbol_to_value_func) (/* unknown */);
int (*ds_symbol_completion_func) (/* unknown */);
int (*ds_help_completion_func) (/* unknown */);
void (*ds_idle_func) (/* unknown */);
int (*ds_check_reserved_name_func) (/* unknown */);
struct _GIF_OPT gif;
struct _GIF_OPT *pgif;
struct _SMEM_OPT smem;
struct _SMEM_OPT *psmem;
int m_result;
word m_sdram;
word m_init;
static struct handle handle_map[256];
