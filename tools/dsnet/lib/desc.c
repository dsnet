/tmp/g/tools/dsnet/lib/desc.c:
DS_DESC_LIST ds_select_list;
static int desc_id;
static int dev_desc_id;
DS_DESC *ds_add_select_list (int type /* 0x8 */, int fd /* 0xc */, char *name /* 0x10 */, int (*accept_func) (/* unknown */) /* 0x14 */, DS_RECV_FUNC *recv_func /* 0x18 */)
{
  {
    int len;
    char *msg;
    DS_DESC *desc;
  }
}
int ds_isttyp (int proto /* 0x8 */)
{
}
DS_RECV_FUNC_DESC *ds_add_recv_func (DS_DESC *desc /* 0x8 */, int proto /* 0xc */, int type /* 0x10 */, int code /* 0x14 */, DS_RECV_FUNC *func /* 0x18 */)
{
  {
    DS_RECV_FUNC_DESC *rf;
  }
}
DS_RECV_FUNC_DESC *ds_del_recv_func (DS_DESC *desc /* 0x8 */, DS_RECV_FUNC_DESC *rf /* 0xc */)
{
}
DS_DESC *ds_close_desc (DS_DESC *desc /* 0x8 */)
{
  {
    DS_RECV_FUNC_DESC *qrf;
    DS_RECV_FUNC_DESC *prf;
    DSP_BUF *q;
    DSP_BUF *p;
  }
}
static int xrecv_common (DS_DESC *desc /* 0x8 */, DSP_BUF *db /* 0xc */)
{
  {
    int code;
    int type;
    int proto;
    int len;
    int r;
    DS_RECV_FUNC_DESC *rf;
    DBGP_HDR *dsdb;
    NETMP_HDR *dsm;
    DCMP_HDR *dsc;
    DECI2_HDR *dh;
  }
}
static int xrecv_kbd (DS_DESC *desc /* 0x8 */)
{
  {
    int n;
    struct { /* size 8 */
      word zero; /* bitsize 32, bitpos 0 */
      byte str[3]; /* bitsize 24, bitpos 32 */
    } dat;
    static byte last;
    DSP_BUF *db;
  }
}
static int xrecv_dev (DS_DESC *desc /* 0x8 */)
{
  {
    int n;
    int r;
    DECI2_HDR *dh;
    DSP_BUF *db;
  }
}
int ds_reset_info (DS_DESC *desc /* 0x8 */)
{
  {
    int r;
  }
}
static int xsend_dev (DS_DESC *desc /* 0x8 */)
{
  {
    int proto;
    int r;
    DECI2_HDR *dh;
    DSP_QUE *dq;
  }
}
static int xrecv_net (DS_DESC *desc /* 0x8 */)
{
  {
    int n;
    int r;
    DECI2_HDR *dh;
    DSP_BUF *db;
  }
}
static int xrecv_comport (DS_DESC *desc /* 0x8 */)
{
  {
    int n;
    struct { /* size 260 */
      word zero; /* bitsize 32, bitpos 0 */
      byte str[256]; /* bitsize 2048, bitpos 32 */
    } dat;
    DECI2_HDR *dh;
    DSP_BUF *db;
  }
}
static int xsend_net (DS_DESC *desc /* 0x8 */)
{
  {
    int proto;
    int r;
    DECI2_HDR *dh;
    DSP_QUE *dq;
  }
}
static int xsend_comport (DS_DESC *desc /* 0x8 */)
{
  {
    int proto;
    int r;
    DECI2_HDR *dh;
    DSP_QUE *dq;
  }
}
static int xsend_netdev (DS_DESC *desc /* 0x8 */)
{
  {
    int proto;
    int r;
    DECI2_HDR *dh;
    DSP_QUE *dq;
  }
}
static int xrecv_netdev (DS_DESC *desc /* 0x8 */)
{
  {
    int n;
    DECI2_HDR *dh;
    DSP_BUF *db;
  }
}
int ds_select_desc (int sec /* 0x8 */, int usec /* 0xc */)
{
  {
    int r;
    struct timeval *tv;
    struct timeval tval;
    fd_set wfds;
    fd_set rfds;
    DS_DESC *q;
    DS_DESC *p;
    {
      int __d1;
      int __d0;
    }
    {
      int __d1;
      int __d0;
    }
    {
      char __result;
    }
    {
      char __result;
    }
  }
}
DS_DESC *ds_desc_by_proto (int proto /* 0x8 */)
{
  {
    int i;
    int cpri;
    NETMP_PROTOS *protos;
    DS_DESC *pri_dp;
    DS_DESC *dp;
  }
}
int ds_isbusy_desc (int pri /* 0x8 */, int proto /* 0xc */)
{
  {
    int i;
    NETMP_PROTOS *protos;
    DS_DESC *dp;
    half proto;
    byte pri;
  }
}
static void ds_flush_tty (DS_DESC *dp /* 0x8 */, int proto /* 0xc */)
{
  {
    char msg[100];
    int nb;
    int node;
    DECI2_HDR *dh;
    DSP_BUF *q;
    DSP_BUF *p;
  }
}
DSP_BUF *ds_send_net (int proto /* 0x8 */, DSP_BUF *db /* 0xc */)
{
  {
    int len;
    int i;
    NETMP_PROTOS *protos;
    DSP_BUF *cp;
    DS_DESC *dp;
  }
}
DSP_BUF *ds_send_desc (DS_DESC *desc /* 0x8 */, DSP_BUF *db /* 0xc */)
{
}
void ds_flush_desc_by_proto (int proto /* 0x8 */)
{
  {
    int nb;
    int np;
    DECI2_HDR *dh;
    DSP_BUF *q;
    DSP_BUF *p;
    DS_DESC *dp;
  }
}
void ds_status_desc ()
{
  {
    int d;
    int usec;
    int sec;
    int i;
    int n;
    NETMP_PROTOS *protos;
    DSP_BUF *db;
    DS_DESC *p;
  }
}
int (*ds_rdwr_mem_align_func) (/* unknown */);
int (*ds_load_mem_func) (/* unknown */);
int (*ds_store_mem_func) (/* unknown */);
int (*ds_load_quad_reg_func) (/* unknown */);
int (*ds_store_quad_reg_func) (/* unknown */);
int (*ds_symbol_to_value_func) (/* unknown */);
int (*ds_symbol_completion_func) (/* unknown */);
int (*ds_help_completion_func) (/* unknown */);
void (*ds_idle_func) (/* unknown */);
int (*ds_check_reserved_name_func) (/* unknown */);
void (*ds_drfp_err_func) (/* unknown */);
struct _GIF_OPT gif;
struct _GIF_OPT *pgif;
struct _SMEM_OPT smem;
struct _SMEM_OPT *psmem;
int m_result;
word m_sdram;
word m_init;
