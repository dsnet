/tmp/g/tools/dsnet/lib/misc.c:
char *ds_program_name;
int (*ds_rdwr_mem_align_func) (/* unknown */);
int (*ds_load_mem_func) (/* unknown */);
int (*ds_store_mem_func) (/* unknown */);
int (*ds_load_quad_reg_func) (/* unknown */);
int (*ds_store_quad_reg_func) (/* unknown */);
int (*ds_symbol_to_value_func) (/* unknown */);
int (*ds_symbol_completion_func) (/* unknown */);
int (*ds_help_completion_func) (/* unknown */);
void (*ds_idle_func) (/* unknown */);
int (*ds_check_reserved_name_func) (/* unknown */);
int ds_rdwr_mem_align (int code /* 0x8 */, int align /* 0xc */, int cpuid /* 0x10 */, int space /* 0x14 */, word adr /* 0x18 */, void *ptr /* 0x1c */, int len /* 0x20 */)
{
  byte space;
  half cpuid;
  byte align;
  byte code;
}
int ds_load_mem (word adr /* 0x8 */, void *ptr /* 0xc */, int len /* 0x10 */)
{
}
int ds_store_mem (word adr /* 0x8 */, void *ptr /* 0xc */, int len /* 0x10 */)
{
}
int ds_load_quad_reg (char *name /* 0x8 */, quad *pq /* 0xc */)
{
}
int ds_store_quad_reg (char *name /* 0x8 */, quad qv /* 0xc */)
{
}
int ds_symbol_to_value (char *name /* 0x8 */, word *pv /* 0xc */)
{
}
int ds_exit (int code /* 0x8 */)
{
}
int ds_exit_invalid_version (char *manager_version /* 0x8 */, int n /* 0xc */, char *client_version /* 0x10 */)
{
  {
    char *p;
  }
}
int ds_fork ()
{
  {
    int pid;
  }
}
static int *pstatus_by_child;
static void sigchld (int arg /* 0x8 */)
{
  {
    int status;
    {
      union { /* size 4 */
        int __in; /* bitsize 32, bitpos 0 */
        int __i; /* bitsize 32, bitpos 0 */
      } __u;
    }
  }
}
int ds_cmd_execution_for_filesv (char *cmd /* 0x8 */, int *pstatus /* 0xc */)
{
  {
    int fd;
    int pid;
    char *shell;
  }
}
int ds_read (int fd /* 0x8 */, void *ptr /* 0xc */, int n /* 0x10 */)
{
  {
    int r;
  }
}
int ds_write (int fd /* 0x8 */, void *ptr /* 0xc */, int n /* 0x10 */)
{
  {
    int r;
  }
}
char *ds_basename (char *name /* 0x8 */)
{
  {
    char *p;
  }
}
char *ds_getenv (char *env /* 0x8 */)
{
}
void ds_bzero (void *ptr /* 0x8 */, int len /* 0xc */)
{
}
int ds_gettime (int *psec /* 0x8 */, int *pusec /* 0xc */)
{
  {
    struct timeval tval;
  }
}
char *ds_strchr (char *str /* 0x8 */, int ch /* 0xc */)
{
}
char *ds_strcat (char *dst /* 0x8 */, char *src /* 0xc */)
{
}
int ds_strncmp (char *s1 /* 0x8 */, char *s2 /* 0xc */, int n /* 0x10 */)
{
}
char *_ds_tilde_expand (char *buf /* 0x8 */, char *str /* 0xc */)
{
  {
    struct passwd *pw;
    char *home;
    char *dp;
    char *sp;
    char user[1024];
  }
}
int ds_gethostname (char *hostname /* 0x8 */, int n /* 0xc */)
{
}
char *ds_getcwd (char *buf /* 0x8 */, int size /* 0xc */)
{
}
char *ds_abs_path (char *buf /* 0x8 */, int len /* 0xc */, char *fname /* 0x10 */)
{
  {
    int n;
  }
}
typedef struct { /* size 268 */
  char buf[256]; /* bitsize 2048, bitpos 0 */
  int put; /* bitsize 32, bitpos 2048 */
  int get; /* bitsize 32, bitpos 2080 */
  int len; /* bitsize 32, bitpos 2112 */
} CQ;
static int read_kbd (int kbd /* 0x8 */, int escape /* 0xc */)
{
  {
    char ch;
    CQ *q;
  }
}
static int read_dev (int dev /* 0x8 */)
{
  {
    char ch;
    CQ *q;
  }
}
static int write_dev (int dev /* 0x8 */)
{
  {
    char ch;
    CQ *q;
  }
}
static int write_dsp (int dsp /* 0x8 */)
{
  {
    char ch;
    CQ *q;
  }
}
DS_DESC *ds_open_comport (char *name /* 0x8 */, DS_RECV_FUNC *recv_func /* 0xc */)
{
  {
    int fd;
  }
}
DS_DESC *ds_open_netdev (char *name /* 0x8 */, DS_RECV_FUNC *recv_func /* 0xc */)
{
  {
    int fd;
  }
}
int ds_comp_main (char *device /* 0x8 */, int escape /* 0xc */)
{
  {
    fd_set wfds;
    fd_set rfds;
    int dsp;
    int kbd;
    int dev;
    {
      int __d1;
      int __d0;
    }
    {
      int __d1;
      int __d0;
    }
    {
      char __result;
    }
    {
      char __result;
    }
    {
      char __result;
    }
    {
      char __result;
    }
  }
}
void (*ds_drfp_err_func) (/* unknown */);
struct _GIF_OPT gif;
struct _GIF_OPT *pgif;
struct _SMEM_OPT smem;
struct _SMEM_OPT *psmem;
int m_result;
word m_sdram;
word m_init;
int ds_errno;
static CQ kbdq;
static CQ devq;
