/tmp/g/tools/dsnet/lib/cmds.c:
struct cmd_entry { /* size 28*/
  struct cmd_entry *forw; /* bitsize 32, bitpos 0 */
  struct cmd_entry *back; /* bitsize 32, bitpos 32 */
  char *name; /* bitsize 32, bitpos 64 */
  char *arg; /* bitsize 32, bitpos 96 */
  char *help; /* bitsize 32, bitpos 128 */
  int (*func) (/* unknown */); /* bitsize 32, bitpos 160 */
  int f_enable; /* bitsize 32, bitpos 192 */
};
typedef struct cmd_entry CMD_ENTRY;
static int cmd_max_na;
DS_OPTION *ds_opt_hex_radix;
DS_OPTION *ds_opt_log_total_size;
DS_OPTION *ds_opt_log_packet_size;
DS_OPTION *ds_opt_histfile;
DS_OPTION *ds_opt_histfilesize;
DS_OPTION *ds_opt_histsize;
DS_OPTION *ds_opt_tty_max_size;
int ds_abort_input;
int ds_cmd_executing;
int ds_shell_cmd (int ac /* 0x8 */, char **av /* 0xc */)
{
  {
    void (*sigint) (/* unknown */);
    int r;
    int fd;
    int raw;
    int status;
    int pid;
    char *bp;
    char buf[1024];
    char *shell;
    {
      int n;
      int i;
    }
    {
      int __status;
      {
        union { /* size 4 */
          int __in; /* bitsize 32, bitpos 0 */
          int __i; /* bitsize 32, bitpos 0 */
        } __u;
      }
    }
  }
}
char *_ds_ref_option_str (char *name /* 0x8 */)
{
  {
    DS_OPTION *p;
  }
}
int _ds_ref_user_variable (char *name /* 0x8 */, word *wp /* 0xc */)
{
  {
    DS_OPTION *p;
  }
}
DS_OPTION *ds_set_option (char *name /* 0x8 */, int type /* 0xc */, char *str /* 0x10 */, int val /* 0x14 */, int f_def /* 0x18 */)
{
  {
    DS_OPTION *q;
    DS_OPTION *p;
  }
}
static void show_option (char *name /* 0x8 */, int f_force /* 0xc */)
{
  {
    char *bp;
    char *indent;
    DS_OPTION *p;
  }
}
static int is_reserved_name (char *name /* 0x8 */)
{
}
int ds_set_cmd (int ac /* 0x8 */, char **av /* 0xc */)
{
  {
    int n;
    int val;
    int f_all;
    char str[1024];
    char name[1024];
    char *bq;
    char *bp;
    DS_OPTION *p;
  }
}
static int read_option (char *fname /* 0x8 */)
{
  {
    char *av[101];
    int cl;
    int ac;
    int size;
    char *sp;
    char *dp;
    char *line;
    char *lp;
    char *pe;
    char *q;
    char *p;
    char *buf;
    DS_FILE *stream;
  }
}
int ds_read_option_file ()
{
  {
    char cwd[4096];
    char fname[4096];
    char *home;
  }
}
int ds_read_startup_file ()
{
  {
    char line[4196];
    char cwd[4096];
    char fname[4096];
    char *home;
  }
}
int ds_source_cmd (int ac /* 0x8 */, char **av /* 0xc */)
{
  {
    static char path[4096];
    int cl;
    int r;
    int size;
    char *sp;
    char *dp;
    char *pe;
    char *q;
    char *p;
    char *line;
    char *buf;
    DS_FILE *stream;
  }
}
int ds_cd_cmd (int ac /* 0x8 */, char **av /* 0xc */)
{
  {
    char path[4096];
    char *dir;
  }
}
static int scan_digit_with_dot (char *str /* 0x8 */, int *psec /* 0xc */, int *pusec /* 0x10 */)
{
  {
    int base;
    int usec;
    int sec;
    char *s;
  }
}
int ds_if_cmd (int ac /* 0x8 */, char **av /* 0xc */)
{
  {
    int n;
    int i;
    int r;
    char *s;
    char *str;
    word wv;
  }
}
int ds_repeat_cmd (int ac /* 0x8 */, char **av /* 0xc */)
{
  {
    int f_i;
    char *cond;
    char *s;
    char *str;
    int r;
    int usec;
    int sec;
    int cnt;
    int n;
    int tus;
    int ts;
    int k;
    int j;
    int i;
    {
      word wv;
    }
  }
}
static int _ds_status_cmd (int ac /* 0x8 */, char **av /* 0xc */)
{
  {
    int f_mem;
    int f_desc;
  }
}
static int _ds_history_cmd (int ac /* 0x8 */, char **av /* 0xc */)
{
  {
    DS_HIST *hp;
  }
}
struct show_arg { /* size 16*/
  struct show_arg *forw; /* bitsize 32, bitpos 0 */
  struct show_arg *back; /* bitsize 32, bitpos 32 */
  char *name; /* bitsize 32, bitpos 64 */
  int (*func) (/* unknown */); /* bitsize 32, bitpos 96 */
};
typedef struct show_arg SHOW_ARG;
static struct { /* size 8 */
  SHOW_ARG *head; /* bitsize 32, bitpos 0 */
  SHOW_ARG *tail; /* bitsize 32, bitpos 32 */
} show_arg_list;
int ds_add_show_arg (char *name /* 0x8 */, int (*func) (/* unknown */) /* 0xc */)
{
  {
    SHOW_ARG *sa;
  }
}
int ds_show_cmd (int ac /* 0x8 */, char **av /* 0xc */)
{
  {
    SHOW_ARG *sa;
    char *tmp[2];
  }
}
int ds_quit_cmd (int ac /* 0x8 */, char **av /* 0xc */)
{
}
struct help_arg { /* size 16*/
  struct help_arg *forw; /* bitsize 32, bitpos 0 */
  struct help_arg *back; /* bitsize 32, bitpos 32 */
  char *name; /* bitsize 32, bitpos 64 */
  int (*func) (/* unknown */); /* bitsize 32, bitpos 96 */
};
typedef struct help_arg HELP_ARG;
static struct { /* size 8 */
  HELP_ARG *head; /* bitsize 32, bitpos 0 */
  HELP_ARG *tail; /* bitsize 32, bitpos 32 */
} help_arg_list;
int ds_add_help (char *name /* 0x8 */, int (*func) (/* unknown */) /* 0xc */)
{
  {
    HELP_ARG *ha;
  }
}
int ds_help_cmd (int ac /* 0x8 */, char **av /* 0xc */)
{
  {
    HELP_ARG *ha;
    int na;
    int n;
    int i;
    int f_all;
    CMD_ENTRY *q;
    CMD_ENTRY *p;
  }
}
void ds_cmd_install (char *name /* 0x8 */, char *arg /* 0xc */, char *help /* 0x10 */, int (*func) (/* unknown */) /* 0x14 */)
{
  {
    int na;
    CMD_ENTRY *p;
  }
}
void ds_cmd_control (char *name /* 0x8 */, int f_enable /* 0xc */)
{
  {
    CMD_ENTRY *p;
  }
}
void ds_opt_standard_init ()
{
  {
    char tmp[1024];
  }
}
void ds_cmd_standard_install (int f_shell /* 0x8 */)
{
  {
    SHOW_ARG *sa;
    static char show_arg[1024];
  }
}
static int ds_enable_cmd (int ac /* 0x8 */, char **av /* 0xc */)
{
  {
    CMD_ENTRY *p;
  }
}
static int ds_disable_cmd (int ac /* 0x8 */, char **av /* 0xc */)
{
  {
    CMD_ENTRY *p;
  }
}
void ds_cmd_xxxable_install ()
{
}
struct alias { /* size 20*/
  struct alias *forw; /* bitsize 32, bitpos 0 */
  struct alias *back; /* bitsize 32, bitpos 32 */
  int used; /* bitsize 32, bitpos 64 */
  char *name; /* bitsize 32, bitpos 96 */
  char *value; /* bitsize 32, bitpos 128 */
};
typedef struct alias ALIAS;
int ds_alias_cmd (int ac /* 0x8 */, char **av /* 0xc */)
{
  {
    char *s;
    int n;
    int i;
    int f_r;
    ALIAS *q;
    ALIAS *p;
  }
}
int ds_cmd_call (int ac /* 0x8 */, char **av /* 0xc */, int f_repeat /* 0x10 */)
{
  {
    int r;
    static int (*last_func) (/* unknown */);
    CMD_ENTRY *p;
  }
}
static ALIAS *alias (char *buf /* 0x8 */, int nbuf /* 0xc */, int ac /* 0x10 */, char **av /* 0x14 */)
{
  {
    int i;
    char *tp;
    char token[1024];
    char *sp;
    char *dp;
    ALIAS *p;
  }
}
static int ds_cmd_execute_internal (char *s /* 0x8 */, int f_repeat /* 0xc */)
{
  {
    ALIAS *p;
    char sbuf[1024];
    char ch;
    char buf[1024];
    char *av[101];
    int ac;
    int r;
  }
}
int ds_cmd_execute (char *s /* 0x8 */, int f_repeat /* 0xc */)
{
  {
    ALIAS *p;
  }
}
static int ds_path_completion (DS_HISTBUF *hb /* 0x8 */, char *path /* 0xc */)
{
  {
    struct stat stbuf;
    int col;
    int r;
    int lns;
    int ns;
    int nm;
    int n;
    struct dirent *dirp;
    DIR *dp;
    char dname[4096];
    char *p;
    char cn[4096];
    char *name;
    char buf[4096];
  }
}
static int ds_symbol_completion (DS_HISTBUF *hb /* 0x8 */, char *name /* 0xc */)
{
}
static int ds_get_last_keyword (char *buf /* 0x8 */, char *str /* 0xc */)
{
  {
    char *p;
    char *s;
  }
}
static int ds_set_completion (DS_HISTBUF *hb /* 0x8 */, char *str /* 0xc */)
{
  {
    int col;
    int lns;
    int ns;
    int nm;
    int n;
    char *cn;
    char *p;
    DS_OPTION *op;
    char name[1024];
  }
}
static int ds_help_completion (DS_HISTBUF *hb /* 0x8 */, char *str /* 0xc */)
{
  {
    int col;
    int lns;
    int ns;
    int nm;
    int n;
    char *cn;
    char *p;
    CMD_ENTRY *cp;
    char name[1024];
  }
}
static int ds_path_or_symbol_completion (DS_HISTBUF *hb /* 0x8 */, char *str /* 0xc */)
{
  {
    int f_path;
    char *p;
    char *s;
    char name[1024];
  }
}
int ds_cmd_completion (DS_HISTBUF *hb /* 0x8 */, char *s /* 0xc */)
{
  {
    int col;
    int lns;
    int ns;
    int nm;
    int n;
    CMD_ENTRY *ce;
    char *cn;
    char *p;
    char name[1024];
  }
}
DSP_BUF *ds_cmd_input (DS_DESC *desc /* 0x8 */, DSP_BUF *db /* 0xc */)
{
  {
    char *p;
    DS_HISTBUF *hb;
    byte *bp;
    byte buf[1024];
    int len;
    DECI2_HDR *dh;
  }
}
DS_HISTBUF ds_histbuf;
int (*ds_rdwr_mem_align_func) (/* unknown */);
int (*ds_load_mem_func) (/* unknown */);
int (*ds_store_mem_func) (/* unknown */);
int (*ds_load_quad_reg_func) (/* unknown */);
int (*ds_store_quad_reg_func) (/* unknown */);
int (*ds_symbol_to_value_func) (/* unknown */);
int (*ds_symbol_completion_func) (/* unknown */);
int (*ds_help_completion_func) (/* unknown */);
void (*ds_idle_func) (/* unknown */);
int (*ds_check_reserved_name_func) (/* unknown */);
void (*ds_drfp_err_func) (/* unknown */);
struct _GIF_OPT gif;
struct _GIF_OPT *pgif;
struct _SMEM_OPT smem;
struct _SMEM_OPT *psmem;
int m_result;
word m_sdram;
word m_init;
static struct { /* size 8 */
  CMD_ENTRY *head; /* bitsize 32, bitpos 0 */
  CMD_ENTRY *tail; /* bitsize 32, bitpos 32 */
} cmd_list;
static struct { /* size 8 */
  DS_OPTION *head; /* bitsize 32, bitpos 0 */
  DS_OPTION *tail; /* bitsize 32, bitpos 32 */
} ds_option_list;
static struct { /* size 8 */
  ALIAS *head; /* bitsize 32, bitpos 0 */
  ALIAS *tail; /* bitsize 32, bitpos 32 */
} aliases;
