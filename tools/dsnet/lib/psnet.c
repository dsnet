/tmp/g/tools/dsnet/lib/psnet.c:
typedef struct { /* size 32 */
  half mag; /* bitsize 16, bitpos 0 */
  half len; /* bitsize 16, bitpos 16 */
  word ctg; /* bitsize 32, bitpos 32 */
  half pri; /* bitsize 16, bitpos 64 */
  half rep; /* bitsize 16, bitpos 80 */
  byte rtag; /* bitsize 8, bitpos 96 */
  byte atag; /* bitsize 8, bitpos 104 */
  byte acode; /* bitsize 8, bitpos 112 */
  byte crsv[5]; /* bitsize 40, bitpos 120 */
  half cid; /* bitsize 16, bitpos 160 */
  half seq; /* bitsize 16, bitpos 176 */
  word req; /* bitsize 32, bitpos 192 */
  word sum; /* bitsize 32, bitpos 224 */
} DS_DECI1;
typedef struct { /* size 80 */
  int cid; /* bitsize 32, bitpos 0 */
  int tseq; /* bitsize 32, bitpos 32 */
  int gseq; /* bitsize 32, bitpos 64 */
  char *snd_buf; /* bitsize 32, bitpos 96 */
  char *snd_ptr; /* bitsize 32, bitpos 128 */
  int snd_len; /* bitsize 32, bitpos 160 */
  int snd_dslen; /* bitsize 32, bitpos 192 */
  DS_DECI1 rcv_hdr; /* bitsize 256, bitpos 224 */
  char *rcv_buf; /* bitsize 32, bitpos 480 */
  char *rcv_ptr; /* bitsize 32, bitpos 512 */
  int rcv_len; /* bitsize 32, bitpos 544 */
  char *read_ptr; /* bitsize 32, bitpos 576 */
  int read_len; /* bitsize 32, bitpos 608 */
} DS_PSNETD_PRIV;
static int set_deci1_hdr (DS_PSNETD_PRIV *priv /* 0x8 */, DS_DECI1 *p /* 0xc */, word req /* 0x10 */, int len /* 0x14 */)
{
  {
    int i;
    word *wp;
    word sum;
  }
}
static DS_PSNETD_PRIV *init_psnet (DS_DESC *desc /* 0x8 */)
{
  {
    int n;
    char *app;
    char *cp;
    DS_PSNETD_PRIV *p;
  }
}
void _ds_free_psnet (DS_DESC *desc /* 0x8 */)
{
  {
    DS_PSNETD_PRIV *p;
  }
}
static int recv_deci1_hook (DS_PSNETD_PRIV *p /* 0x8 */, DS_DECI1 *deci1 /* 0xc */, void *ptr /* 0x10 */, int len /* 0x14 */)
{
  {
    NETMP_HDR *mh;
    DECI2_HDR *dh;
    word result;
    word req;
  }
}
int _ds_read_psnet (DS_DESC *desc /* 0x8 */, char *buf /* 0xc */, int len /* 0x10 */)
{
  {
    word sum;
    word *wp;
    int deci1_len;
    int n;
    int i;
    int r;
    DS_DECI1 *deci1;
    DS_PSNETD_PRIV *p;
  }
}
static int send_treset (DS_PSNETD_PRIV *p /* 0x8 */)
{
  {
    char *cp;
  }
}
static int send_deci2_hook (DS_PSNETD_PRIV *p /* 0x8 */, DECI2_HDR *dh /* 0xc */, int len /* 0x10 */)
{
  {
    NETMP_HDR *mh;
  }
}
int _ds_write_psnet (DS_DESC *desc /* 0x8 */, char *buf /* 0xc */, int len /* 0x10 */)
{
  {
    char *dp;
    int n;
    int r;
    DS_DECI1 *deci1;
    DS_PSNETD_PRIV *p;
  }
}
int (*ds_rdwr_mem_align_func) (/* unknown */);
int (*ds_load_mem_func) (/* unknown */);
int (*ds_store_mem_func) (/* unknown */);
int (*ds_load_quad_reg_func) (/* unknown */);
int (*ds_store_quad_reg_func) (/* unknown */);
int (*ds_symbol_to_value_func) (/* unknown */);
int (*ds_symbol_completion_func) (/* unknown */);
int (*ds_help_completion_func) (/* unknown */);
void (*ds_idle_func) (/* unknown */);
int (*ds_check_reserved_name_func) (/* unknown */);
void (*ds_drfp_err_func) (/* unknown */);
struct _GIF_OPT gif;
struct _GIF_OPT *pgif;
struct _SMEM_OPT smem;
struct _SMEM_OPT *psmem;
int m_result;
word m_sdram;
word m_init;
