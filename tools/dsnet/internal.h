 internal.h:
typedef struct { /* size 12 */
  char *name; /* bitsize 32, bitpos 0 */
  char *sname; /* bitsize 32, bitpos 32 */
  int value; /* bitsize 32, bitpos 64 */
} STRS;
typedef struct { /* size 16 */
  char *def_fmt; /* bitsize 32, bitpos 0 */
  char *ds_fmt; /* bitsize 32, bitpos 32 */
  STRS *strs; /* bitsize 32, bitpos 64 */
  int nent; /* bitsize 32, bitpos 96 */
} FMT_STRS;
typedef struct { /* size 8 */
  DS_DESC *head; /* bitsize 32, bitpos 0 */
  DS_DESC *tail; /* bitsize 32, bitpos 32 */
} DS_DESC_LIST;
