 ../include/padiag.h:
struct _GIF_OPT { /* size 76*/
  unsigned char *m_buff1; /* bitsize 32, bitpos 0 */
  unsigned char *m_buff2; /* bitsize 32, bitpos 32 */
  unsigned char *m_buff3; /* bitsize 32, bitpos 64 */
  unsigned char *m_buff4; /* bitsize 32, bitpos 96 */
  int m_size1; /* bitsize 32, bitpos 128 */
  int m_size2; /* bitsize 32, bitpos 160 */
  int m_size3; /* bitsize 32, bitpos 192 */
  int m_size4; /* bitsize 32, bitpos 224 */
  int m_count1; /* bitsize 32, bitpos 256 */
  int m_count2; /* bitsize 32, bitpos 288 */
  int m_count3; /* bitsize 32, bitpos 320 */
  int m_count4; /* bitsize 32, bitpos 352 */
  int m_error1; /* bitsize 32, bitpos 384 */
  int m_error2; /* bitsize 32, bitpos 416 */
  int m_error3; /* bitsize 32, bitpos 448 */
  int m_error4; /* bitsize 32, bitpos 480 */
  int m_flag; /* bitsize 32, bitpos 512 */
  int m_complete; /* bitsize 32, bitpos 544 */
  word m_sdram; /* bitsize 32, bitpos 576 */
};
struct _SMEM_OPT { /* size 32*/
  int row_addr; /* bitsize 32, bitpos 0 */
  int col_addr; /* bitsize 32, bitpos 32 */
  int read_row_addr; /* bitsize 32, bitpos 64 */
  int read_col_addr; /* bitsize 32, bitpos 96 */
  int checkmode; /* bitsize 32, bitpos 128 */
  int checkEnable; /* bitsize 32, bitpos 160 */
  int checkdata; /* bitsize 32, bitpos 192 */
  int m_complete; /* bitsize 32, bitpos 224 */
};
struct VERIFY_SSS { /* size 784*/
  char *diagname; /* bitsize 32, bitpos 0 */
  int dev; /* bitsize 32, bitpos 32 */
  int bus_type; /* bitsize 32, bitpos 64 */
  int type; /* bitsize 32, bitpos 96 */
  int step; /* bitsize 32, bitpos 128 */
  int status; /* bitsize 32, bitpos 160 */
  int sta_int; /* bitsize 32, bitpos 192 */
  int sta_ube; /* bitsize 32, bitpos 224 */
  int sta_wait; /* bitsize 32, bitpos 256 */
  int cnt_case[99]; /* bitsize 3168, bitpos 288 */
  int cnt; /* bitsize 32, bitpos 3456 */
  int cnt_rd; /* bitsize 32, bitpos 3488 */
  int cnt_wr; /* bitsize 32, bitpos 3520 */
  int cnt_dmar; /* bitsize 32, bitpos 3552 */
  int cnt_dmaw; /* bitsize 32, bitpos 3584 */
  int cnt_int; /* bitsize 32, bitpos 3616 */
  int cnt_ube; /* bitsize 32, bitpos 3648 */
  int cnt_wait; /* bitsize 32, bitpos 3680 */
  int flg_wr; /* bitsize 32, bitpos 3712 */
  int flg_rd; /* bitsize 32, bitpos 3744 */
  int flg_dmaw; /* bitsize 32, bitpos 3776 */
  int flg_dmar; /* bitsize 32, bitpos 3808 */
  int flg_int; /* bitsize 32, bitpos 3840 */
  int flg_ube; /* bitsize 32, bitpos 3872 */
  int flg_wait; /* bitsize 32, bitpos 3904 */
  unsigned int ref_ROW; /* bitsize 32, bitpos 3936 */
  unsigned int ref_COL; /* bitsize 32, bitpos 3968 */
  unsigned int ref_WR; /* bitsize 32, bitpos 4000 */
  unsigned int bef_ROW; /* bitsize 32, bitpos 4032 */
  unsigned int bef_COL; /* bitsize 32, bitpos 4064 */
  unsigned char COL_flag0[256]; /* bitsize 2048, bitpos 4096 */
  unsigned char COL_flag1[4]; /* bitsize 32, bitpos 6144 */
  unsigned char ROW_flag[2]; /* bitsize 16, bitpos 6176 */
  unsigned int okCount; /* bitsize 32, bitpos 6208 */
  unsigned int ngCount; /* bitsize 32, bitpos 6240 */
};
typedef struct VERIFY_SSS VERIFY_SSS;
typedef struct VERIFY_SSS *PVERIFY_SSS;
struct VERIFY_PFM { /* size 8*/
  int status; /* bitsize 32, bitpos 0 */
  long unsigned int cnt; /* bitsize 32, bitpos 32 */
};
typedef struct VERIFY_PFM VERIFY_PFM;
typedef struct VERIFY_PFM *PVERIFY_PFM;
