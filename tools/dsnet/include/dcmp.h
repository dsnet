 ../include/dcmp.h:
typedef struct { /* size 4 */
  byte type; /* bitsize 8, bitpos 0 */
  byte code; /* bitsize 8, bitpos 8 */
  half unused; /* bitsize 16, bitpos 16 */
} DCMP_HDR;
typedef struct { /* size 4 */
  byte result; /* bitsize 8, bitpos 0 */
  byte unused[3]; /* bitsize 24, bitpos 8 */
} DCMP_CONNECT_DATA;
typedef struct { /* size 16 */
  word param[4]; /* bitsize 128, bitpos 0 */
} DCMP_BOOTP;
typedef struct { /* size 4 */
  half id; /* bitsize 16, bitpos 0 */
  half seq; /* bitsize 16, bitpos 16 */
} DCMP_ECHO_DATA;
typedef struct { /* size 2 */
  half proto; /* bitsize 16, bitpos 0 */
} DCMP_STATUS_DATA;
typedef struct { /* size 24 */
  DECI2_HDR orig_hdr; /* bitsize 64, bitpos 0 */
  byte orig_data[16]; /* bitsize 128, bitpos 64 */
} DCMP_ERROR_DATA;
