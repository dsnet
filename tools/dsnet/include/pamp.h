 ../include/pamp.h:
typedef word PAMP_HDR;
typedef struct { /* size 16 */
  word a; /* bitsize 32, bitpos 0 */
  word b; /* bitsize 32, bitpos 32 */
  word c; /* bitsize 32, bitpos 64 */
  word d; /* bitsize 32, bitpos 96 */
} TRIG_SELECT;
typedef struct { /* size 24 */
  word address; /* bitsize 32, bitpos 0 */
  word addressmask; /* bitsize 32, bitpos 32 */
  word hidata; /* bitsize 32, bitpos 64 */
  word lodata; /* bitsize 32, bitpos 96 */
  word hidatamask; /* bitsize 32, bitpos 128 */
  word lodatamask; /* bitsize 32, bitpos 160 */
} TRIG_REG;
typedef struct { /* size 10 */
  unsigned char GINT; /* bitsize 1, bitpos 0 */
  unsigned char GRDYGC; /* bitsize 1, bitpos 1 */
  unsigned char GVLDGC; /* bitsize 1, bitpos 2 */
  unsigned char GRDYCG; /* bitsize 1, bitpos 3 */
  unsigned char GVLDCG; /* bitsize 1, bitpos 4 */
  unsigned char GRW; /* bitsize 1, bitpos 5 */
  unsigned char GACK; /* bitsize 1, bitpos 6 */
  unsigned char GREQ; /* bitsize 1, bitpos 7 */
  unsigned char GA; /* bitsize 7, bitpos 8 */
  unsigned char VBLANK; /* bitsize 1, bitpos 15 */
  unsigned int GDL; /* bitsize 32, bitpos 16 */
  unsigned int GDH; /* bitsize 32, bitpos 48 */
} gifdata_t;
typedef struct { /* size 10 */
  unsigned char Ap0a; /* bitsize 1, bitpos 0 */
  unsigned char Ap0b; /* bitsize 1, bitpos 1 */
  unsigned char Ap1a; /* bitsize 1, bitpos 2 */
  unsigned char Ap1b; /* bitsize 1, bitpos 3 */
  unsigned char Aja; /* bitsize 1, bitpos 4 */
  unsigned char Ajb; /* bitsize 1, bitpos 5 */
  unsigned char Atpce; /* bitsize 1, bitpos 6 */
  unsigned char Atpc0; /* bitsize 1, bitpos 7 */
  unsigned char Atpc1; /* bitsize 1, bitpos 8 */
  unsigned char Atpc2; /* bitsize 1, bitpos 9 */
  unsigned char Atpc3; /* bitsize 1, bitpos 10 */
  unsigned char Atrig; /* bitsize 1, bitpos 11 */
  unsigned char Ambst0; /* bitsize 1, bitpos 12 */
  unsigned char Ambst1; /* bitsize 1, bitpos 13 */
  unsigned char Ambst2; /* bitsize 1, bitpos 14 */
  unsigned char Ambst3; /* bitsize 1, bitpos 15 */
  unsigned char Ambst4; /* bitsize 1, bitpos 16 */
  unsigned char Ambst5; /* bitsize 1, bitpos 17 */
  unsigned char Ambsnd; /* bitsize 1, bitpos 18 */
  unsigned char Av0a; /* bitsize 1, bitpos 19 */
  unsigned char Av0b; /* bitsize 1, bitpos 20 */
  unsigned char Av1a; /* bitsize 1, bitpos 21 */
  unsigned char Av1b; /* bitsize 1, bitpos 22 */
  unsigned char Av1xa; /* bitsize 1, bitpos 23 */
  unsigned char Av1xb; /* bitsize 1, bitpos 24 */
  unsigned char Agifdt0; /* bitsize 1, bitpos 25 */
  unsigned char Agifdt1; /* bitsize 1, bitpos 26 */
  unsigned char Apgn0; /* bitsize 1, bitpos 27 */
  unsigned char Apgn1; /* bitsize 1, bitpos 28 */
  unsigned char Apgn2; /* bitsize 1, bitpos 29 */
  unsigned char Apgn3; /* bitsize 1, bitpos 30 */
  unsigned char Apgn4; /* bitsize 1, bitpos 31 */
  unsigned char Asrv; /* bitsize 1, bitpos 32 */
  unsigned char Amrd0; /* bitsize 1, bitpos 33 */
  unsigned char Amrd1; /* bitsize 1, bitpos 34 */
  unsigned char Amwt0; /* bitsize 1, bitpos 35 */
  unsigned char Amwt1; /* bitsize 1, bitpos 36 */
  unsigned char Apms0; /* bitsize 1, bitpos 37 */
  unsigned char Apms1; /* bitsize 1, bitpos 38 */
  unsigned char Apwt0; /* bitsize 1, bitpos 39 */
  unsigned char Apwt1; /* bitsize 1, bitpos 40 */
  unsigned char Apwt2; /* bitsize 1, bitpos 41 */
  unsigned char Apwt3; /* bitsize 1, bitpos 42 */
  unsigned char Attr0; /* bitsize 1, bitpos 43 */
  unsigned char Attr1; /* bitsize 1, bitpos 44 */
  unsigned char Actr; /* bitsize 1, bitpos 45 */
  unsigned char Ammd0; /* bitsize 1, bitpos 46 */
  unsigned char Ammd1; /* bitsize 1, bitpos 47 */
  unsigned char Ammd2; /* bitsize 1, bitpos 48 */
  unsigned char Adisp; /* bitsize 1, bitpos 49 */
  unsigned char Atrd0; /* bitsize 1, bitpos 50 */
  unsigned char Atrd1; /* bitsize 1, bitpos 51 */
  unsigned char Atrd2; /* bitsize 1, bitpos 52 */
  unsigned char Atrd3; /* bitsize 1, bitpos 53 */
  unsigned char Atrd4; /* bitsize 1, bitpos 54 */
  unsigned char Amrd2; /* bitsize 1, bitpos 55 */
  unsigned char Amrd3; /* bitsize 1, bitpos 56 */
  unsigned char Amwt2; /* bitsize 1, bitpos 57 */
  unsigned char Amwt3; /* bitsize 1, bitpos 58 */
  unsigned char Apms2; /* bitsize 1, bitpos 59 */
  unsigned char Apms3; /* bitsize 1, bitpos 60 */
  unsigned char Apwt4; /* bitsize 1, bitpos 61 */
  unsigned char Apwt5; /* bitsize 1, bitpos 62 */
  unsigned char Apwt6; /* bitsize 1, bitpos 63 */
  unsigned char Apwt7; /* bitsize 1, bitpos 64 */
  unsigned char Attr2; /* bitsize 1, bitpos 65 */
  unsigned char Attr3; /* bitsize 1, bitpos 66 */
  unsigned char Areserve2; /* bitsize 5, bitpos 67 */
  unsigned char Areserve3; /* bitsize 8, bitpos 72 */
} eegsdata_t;
typedef struct { /* size 20 */
  word bid; /* bitsize 32, bitpos 0 */
  word master; /* bitsize 32, bitpos 32 */
  word eegs; /* bitsize 32, bitpos 64 */
  word gif; /* bitsize 32, bitpos 96 */
  word iop; /* bitsize 32, bitpos 128 */
} paver_t;
typedef struct { /* size 24 */
  word addr; /* bitsize 32, bitpos 0 */
  word amsk; /* bitsize 32, bitpos 32 */
  word hidata; /* bitsize 32, bitpos 64 */
  word lodata; /* bitsize 32, bitpos 96 */
  word hidmsk; /* bitsize 32, bitpos 128 */
  word lodmsk; /* bitsize 32, bitpos 160 */
} gifreg_t;
typedef struct { /* size 16 */
  word addr; /* bitsize 24, bitpos 0 */
  word spu2core0; /* bitsize 1, bitpos 24 */
  word spu2core1; /* bitsize 1, bitpos 25 */
  word cdrom; /* bitsize 1, bitpos 26 */
  word dev9; /* bitsize 1, bitpos 27 */
  word sif; /* bitsize 1, bitpos 28 */
  word read; /* bitsize 1, bitpos 29 */
  word write; /* bitsize 1, bitpos 30 */
  word enable; /* bitsize 1, bitpos 31 */
  word addrmask; /* bitsize 32, bitpos 32 */
  word data; /* bitsize 32, bitpos 64 */
  word datamask; /* bitsize 32, bitpos 96 */
} iopmem_t;
typedef struct { /* size 16 */
  unsigned int MD; /* bitsize 32, bitpos 0 */
  unsigned int AD; /* bitsize 11, bitpos 32 */
  unsigned int MRAS; /* bitsize 1, bitpos 43 */
  unsigned int MCAS; /* bitsize 4, bitpos 44 */
  unsigned int RA; /* bitsize 11, bitpos 48 */
  unsigned int _rsv0; /* bitsize 1, bitpos 59 */
  unsigned int MWE; /* bitsize 1, bitpos 60 */
  unsigned int _rsv1; /* bitsize 1, bitpos 61 */
  unsigned int SMWR; /* bitsize 1, bitpos 62 */
  unsigned int SMEMAC; /* bitsize 1, bitpos 63 */
  unsigned int SSRST; /* bitsize 1, bitpos 64 */
  unsigned int WAIT; /* bitsize 1, bitpos 65 */
  unsigned int UBE; /* bitsize 1, bitpos 66 */
  unsigned int SWR; /* bitsize 1, bitpos 67 */
  unsigned int SRD; /* bitsize 1, bitpos 68 */
  unsigned int RT; /* bitsize 1, bitpos 69 */
  unsigned int DACKEX; /* bitsize 1, bitpos 70 */
  unsigned int DACK9; /* bitsize 1, bitpos 71 */
  unsigned int DACK8; /* bitsize 1, bitpos 72 */
  unsigned int DACK5; /* bitsize 1, bitpos 73 */
  unsigned int DACK4; /* bitsize 1, bitpos 74 */
  unsigned int SIFTYPE; /* bitsize 5, bitpos 75 */
  unsigned int INTEX; /* bitsize 1, bitpos 80 */
  unsigned int INT9; /* bitsize 1, bitpos 81 */
  unsigned int INT8; /* bitsize 1, bitpos 82 */
  unsigned int INT5; /* bitsize 1, bitpos 83 */
  unsigned int INT4; /* bitsize 1, bitpos 84 */
  unsigned int EXTR; /* bitsize 1, bitpos 85 */
  unsigned int CS9C; /* bitsize 1, bitpos 86 */
  unsigned int CS9; /* bitsize 1, bitpos 87 */
  unsigned int cs8; /* bitsize 1, bitpos 88 */
  unsigned int cs5; /* bitsize 1, bitpos 89 */
  unsigned int cs4; /* bitsize 1, bitpos 90 */
  unsigned int cs1; /* bitsize 1, bitpos 91 */
  unsigned int cs2; /* bitsize 1, bitpos 92 */
  unsigned int IOIS16; /* bitsize 1, bitpos 93 */
  unsigned int SSWR; /* bitsize 1, bitpos 94 */
  unsigned int SSAC; /* bitsize 1, bitpos 95 */
  unsigned int SRST; /* bitsize 1, bitpos 96 */
  unsigned int SINT; /* bitsize 1, bitpos 97 */
  unsigned int GINT; /* bitsize 1, bitpos 98 */
  unsigned int DACK; /* bitsize 1, bitpos 99 */
  unsigned int DREQ0; /* bitsize 1, bitpos 100 */
  unsigned int DREQ1; /* bitsize 1, bitpos 101 */
  unsigned int BGNT; /* bitsize 1, bitpos 102 */
  unsigned int BREQ; /* bitsize 1, bitpos 103 */
  unsigned int RDY; /* bitsize 1, bitpos 104 */
  unsigned int WRAC; /* bitsize 1, bitpos 105 */
  unsigned int RDAC; /* bitsize 1, bitpos 106 */
  unsigned int BE0; /* bitsize 1, bitpos 107 */
  unsigned int BE1; /* bitsize 1, bitpos 108 */
  unsigned int BE2; /* bitsize 1, bitpos 109 */
  unsigned int BE3; /* bitsize 1, bitpos 110 */
  unsigned int _rsv2; /* bitsize 1, bitpos 111 */
  unsigned int HBLK1; /* bitsize 1, bitpos 112 */
  unsigned int VBLK1; /* bitsize 1, bitpos 113 */
  unsigned int _rsv3; /* bitsize 1, bitpos 114 */
  unsigned int _rsv4; /* bitsize 1, bitpos 115 */
  unsigned int DMAPIO; /* bitsize 2, bitpos 116 */
  unsigned int DIR; /* bitsize 1, bitpos 118 */
  unsigned int SIFAC; /* bitsize 1, bitpos 119 */
  unsigned int _rsv; /* bitsize 8, bitpos 120 */
} iopfpga_t;
typedef struct { /* size 16 */
  unsigned int MD; /* bitsize 32, bitpos 0 */
  unsigned int AD; /* bitsize 23, bitpos 32 */
  unsigned int MCAS; /* bitsize 4, bitpos 55 */
  unsigned int REFRESH; /* bitsize 1, bitpos 59 */
  unsigned int MRAS; /* bitsize 1, bitpos 60 */
  unsigned int _rsv0; /* bitsize 1, bitpos 61 */
  unsigned int SMWR; /* bitsize 1, bitpos 62 */
  unsigned int SMEMAC; /* bitsize 1, bitpos 63 */
  unsigned int _rsv1; /* bitsize 6, bitpos 64 */
  unsigned int DACK_INTERNAL; /* bitsize 1, bitpos 70 */
  unsigned int DACK_DEV9; /* bitsize 1, bitpos 71 */
  unsigned int DACK_SPU2_1; /* bitsize 1, bitpos 72 */
  unsigned int DACK_CDROM; /* bitsize 1, bitpos 73 */
  unsigned int DACK_SPU2_0; /* bitsize 1, bitpos 74 */
  unsigned int _rsv2; /* bitsize 5, bitpos 75 */
  unsigned int INTEX; /* bitsize 1, bitpos 80 */
  unsigned int INT9; /* bitsize 1, bitpos 81 */
  unsigned int INT8; /* bitsize 1, bitpos 82 */
  unsigned int INT5; /* bitsize 1, bitpos 83 */
  unsigned int INT4; /* bitsize 1, bitpos 84 */
  unsigned int EXTR; /* bitsize 1, bitpos 85 */
  unsigned int _rsv3; /* bitsize 1, bitpos 86 */
  unsigned int CS9; /* bitsize 1, bitpos 87 */
  unsigned int cs8; /* bitsize 1, bitpos 88 */
  unsigned int cs5; /* bitsize 1, bitpos 89 */
  unsigned int cs4; /* bitsize 1, bitpos 90 */
  unsigned int _rsv4; /* bitsize 1, bitpos 91 */
  unsigned int cs2; /* bitsize 1, bitpos 92 */
  unsigned int _rsv5; /* bitsize 1, bitpos 93 */
  unsigned int SSWR; /* bitsize 1, bitpos 94 */
  unsigned int SSAC; /* bitsize 1, bitpos 95 */
  unsigned int _rsv6; /* bitsize 16, bitpos 96 */
  unsigned int HBLK1; /* bitsize 1, bitpos 112 */
  unsigned int VBLK1; /* bitsize 1, bitpos 113 */
  unsigned int _rsv7; /* bitsize 2, bitpos 114 */
  unsigned int DMACH; /* bitsize 2, bitpos 116 */
  unsigned int DMADIR; /* bitsize 1, bitpos 118 */
  unsigned int _rsv8; /* bitsize 5, bitpos 119 */
  unsigned int TRIG_B; /* bitsize 1, bitpos 124 */
  unsigned int TRIG_A; /* bitsize 1, bitpos 125 */
  unsigned int _rsv9; /* bitsize 1, bitpos 126 */
  unsigned int MTRIG; /* bitsize 1, bitpos 127 */
} iopdata_t;
