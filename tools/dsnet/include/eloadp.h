 ../include/eloadp.h:
typedef struct { /* size 8 */
  byte cmd; /* bitsize 8, bitpos 0 */
  byte action; /* bitsize 8, bitpos 8 */
  byte result; /* bitsize 8, bitpos 16 */
  byte stamp; /* bitsize 8, bitpos 24 */
  word module_id; /* bitsize 32, bitpos 32 */
} ELOADP_HDR;
typedef struct { /* size 32 */
  half version; /* bitsize 16, bitpos 0 */
  half flags; /* bitsize 16, bitpos 16 */
  word mod_addr; /* bitsize 32, bitpos 32 */
  word text_size; /* bitsize 32, bitpos 64 */
  word data_size; /* bitsize 32, bitpos 96 */
  word bss_size; /* bitsize 32, bitpos 128 */
  word entry_address; /* bitsize 32, bitpos 160 */
  word gp_value; /* bitsize 32, bitpos 192 */
  byte extnumwords; /* bitsize 8, bitpos 224 */
  byte exttype; /* bitsize 8, bitpos 232 */
  half reserved; /* bitsize 16, bitpos 240 */
  word extword[]; /* bitpos 256 */
} ELOADP_MODINFO_DATA;
typedef struct { /* size 4 */
  word ret_value; /* bitsize 32, bitpos 0 */
} ELOADP_REPORT_DATA;
