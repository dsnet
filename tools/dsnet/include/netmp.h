 ../include/netmp.h:
typedef struct { /* size 2 */
  byte code; /* bitsize 8, bitpos 0 */
  byte result; /* bitsize 8, bitpos 8 */
} NETMP_HDR;
typedef struct { /* size 34 */
  half reserved; /* bitsize 16, bitpos 0 */
  byte param[32]; /* bitsize 256, bitpos 16 */
} NETMP_BOOTP;
typedef struct { /* size 4 */
  byte pri; /* bitsize 8, bitpos 0 */
  byte reserved; /* bitsize 8, bitpos 8 */
  half proto; /* bitsize 16, bitpos 16 */
} NETMP_PROTOS;
typedef struct { /* size 8 */
  NETMP_PROTOS protos; /* bitsize 32, bitpos 0 */
  word connect_time; /* bitsize 32, bitpos 32 */
} NETMP_STATUS_DATA;
