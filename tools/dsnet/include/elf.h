 ../include/elf.h:
typedef struct { /* size 52 */
  byte ident[16]; /* bitsize 128, bitpos 0 */
  half type; /* bitsize 16, bitpos 128 */
  half machine; /* bitsize 16, bitpos 144 */
  word version; /* bitsize 32, bitpos 160 */
  word entry; /* bitsize 32, bitpos 192 */
  word phoff; /* bitsize 32, bitpos 224 */
  word shoff; /* bitsize 32, bitpos 256 */
  word flags; /* bitsize 32, bitpos 288 */
  half ehsize; /* bitsize 16, bitpos 320 */
  half phentsize; /* bitsize 16, bitpos 336 */
  half phnum; /* bitsize 16, bitpos 352 */
  half shentsize; /* bitsize 16, bitpos 368 */
  half shnum; /* bitsize 16, bitpos 384 */
  half shstrndx; /* bitsize 16, bitpos 400 */
} DS_ELF_EHDR;
typedef struct { /* size 32 */
  word type; /* bitsize 32, bitpos 0 */
  word offset; /* bitsize 32, bitpos 32 */
  word vaddr; /* bitsize 32, bitpos 64 */
  word paddr; /* bitsize 32, bitpos 96 */
  word filesz; /* bitsize 32, bitpos 128 */
  word memsz; /* bitsize 32, bitpos 160 */
  word flags; /* bitsize 32, bitpos 192 */
  word align; /* bitsize 32, bitpos 224 */
} DS_ELF_PHDR;
typedef struct { /* size 40 */
  word name; /* bitsize 32, bitpos 0 */
  word type; /* bitsize 32, bitpos 32 */
  word flags; /* bitsize 32, bitpos 64 */
  word addr; /* bitsize 32, bitpos 96 */
  word offset; /* bitsize 32, bitpos 128 */
  word size; /* bitsize 32, bitpos 160 */
  word link; /* bitsize 32, bitpos 192 */
  word info; /* bitsize 32, bitpos 224 */
  word addralign; /* bitsize 32, bitpos 256 */
  word entsize; /* bitsize 32, bitpos 288 */
} DS_ELF_SHDR;
typedef struct { /* size 24 */
  word gprmask; /* bitsize 32, bitpos 0 */
  word cprmask[4]; /* bitsize 128, bitpos 32 */
  word gp_value; /* bitsize 32, bitpos 160 */
} DS_ELF_REGINFO;
typedef struct { /* size 16 */
  word name; /* bitsize 32, bitpos 0 */
  word value; /* bitsize 32, bitpos 32 */
  word size; /* bitsize 32, bitpos 64 */
  byte info; /* bitsize 8, bitpos 96 */
  byte other; /* bitsize 8, bitpos 104 */
  half shndx; /* bitsize 16, bitpos 112 */
} DS_ELF_SYMTAB;
typedef struct { /* size 28 */
  word moduleinfo; /* bitsize 32, bitpos 0 */
  word entry; /* bitsize 32, bitpos 32 */
  word gp_value; /* bitsize 32, bitpos 64 */
  word text_size; /* bitsize 32, bitpos 96 */
  word data_size; /* bitsize 32, bitpos 128 */
  word bss_size; /* bitsize 32, bitpos 160 */
  half moduleversion; /* bitsize 16, bitpos 192 */
  char modulename[1]; /* bitsize 8, bitpos 208 */
} IOPMOD;
typedef struct { /* size 44 */
  word moduleinfo; /* bitsize 32, bitpos 0 */
  word entry; /* bitsize 32, bitpos 32 */
  word gp_value; /* bitsize 32, bitpos 64 */
  word text_size; /* bitsize 32, bitpos 96 */
  word data_size; /* bitsize 32, bitpos 128 */
  word bss_size; /* bitsize 32, bitpos 160 */
  word erx_lib_addr; /* bitsize 32, bitpos 192 */
  word erx_lib_size; /* bitsize 32, bitpos 224 */
  word erx_stub_addr; /* bitsize 32, bitpos 256 */
  word erx_stub_size; /* bitsize 32, bitpos 288 */
  half moduleversion; /* bitsize 16, bitpos 320 */
  char modulename[1]; /* bitsize 8, bitpos 336 */
} EEMOD;
