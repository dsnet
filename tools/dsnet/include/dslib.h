 ../include/dslib.h:
typedef unsigned char byte;
typedef short unsigned int half;
typedef unsigned int word;
typedef struct { /* size 8 */
  word xa[2]; /* bitsize 64, bitpos 0 */
} twin;
typedef struct { /* size 16 */
  word xa[4]; /* bitsize 128, bitpos 0 */
} quad;
struct _dsp_buf { /* size 8*/
  struct _dsp_buf *forw; /* bitsize 32, bitpos 0 */
  struct _dsp_buf *back; /* bitsize 32, bitpos 32 */
};
typedef struct _dsp_buf DSP_BUF;
typedef struct { /* size 8 */
  DSP_BUF *head; /* bitsize 32, bitpos 0 */
  DSP_BUF *tail; /* bitsize 32, bitpos 32 */
} DSP_QUE;
struct _ds_recv_func_desc { /* size 24*/
  struct _ds_recv_func_desc *forw; /* bitsize 32, bitpos 0 */
  struct _ds_recv_func_desc *back; /* bitsize 32, bitpos 32 */
  int proto; /* bitsize 32, bitpos 64 */
  int type; /* bitsize 32, bitpos 96 */
  int code; /* bitsize 32, bitpos 128 */
  DSP_BUF *(*func) (/* unknown */); /* bitsize 32, bitpos 160 */
};
struct _ds_desc { /* size 108*/
  struct _ds_desc *forw; /* bitsize 32, bitpos 0 */
  struct _ds_desc *back; /* bitsize 32, bitpos 32 */
  int id; /* bitsize 32, bitpos 64 */
  int type; /* bitsize 32, bitpos 96 */
  int f_psnet; /* bitsize 32, bitpos 128 */
  void *psnet_priv; /* bitsize 32, bitpos 160 */
  int fd; /* bitsize 32, bitpos 192 */
  DSP_QUE sque; /* bitsize 64, bitpos 224 */
  DECI2_HDR rhdr; /* bitsize 64, bitpos 288 */
  char *sptr; /* bitsize 32, bitpos 352 */
  char *rptr; /* bitsize 32, bitpos 384 */
  int slen; /* bitsize 32, bitpos 416 */
  int rlen; /* bitsize 32, bitpos 448 */
  DSP_BUF *sbuf; /* bitsize 32, bitpos 480 */
  DSP_BUF *rbuf; /* bitsize 32, bitpos 512 */
  int sec; /* bitsize 32, bitpos 544 */
  int usec; /* bitsize 32, bitpos 576 */
  int comport; /* bitsize 32, bitpos 608 */
  char *msg; /* bitsize 32, bitpos 640 */
  int tty_len; /* bitsize 32, bitpos 672 */
  struct { /* size 8 */
    struct _ds_recv_func_desc *head; /* bitsize 32, bitpos 0 */
    struct _ds_recv_func_desc *tail; /* bitsize 32, bitpos 32 */
  } recv_func_list; /* bitsize 64, bitpos 704 */
  int (*accept_func) (/* unknown */); /* bitsize 32, bitpos 768 */
  NETMP_PROTOS *protos; /* bitsize 32, bitpos 800 */
  int nprotos; /* bitsize 32, bitpos 832 */
};
typedef struct _ds_desc DS_DESC;
typedef struct _ds_recv_func_desc DS_RECV_FUNC_DESC;
typedef DSP_BUF *(DS_RECV_FUNC) (/* unknown */);
struct _ds_hist { /* size 16*/
  struct _ds_hist *forw; /* bitsize 32, bitpos 0 */
  struct _ds_hist *back; /* bitsize 32, bitpos 32 */
  int no; /* bitsize 32, bitpos 64 */
  char buf[1]; /* bitsize 8, bitpos 96 */
};
typedef struct _ds_hist DS_HIST;
typedef struct { /* size 2068 */
  DS_HIST *head; /* bitsize 32, bitpos 0 */
  DS_HIST *tail; /* bitsize 32, bitpos 32 */
  DS_HIST *curr; /* bitsize 32, bitpos 64 */
  int no; /* bitsize 32, bitpos 96 */
  char buf[1024]; /* bitsize 8192, bitpos 128 */
  char *ptr; /* bitsize 32, bitpos 8320 */
  char yank[1024]; /* bitsize 8192, bitpos 8352 */
} DS_HISTBUF;
struct _ds_option { /* size 32*/
  struct _ds_option *forw; /* bitsize 32, bitpos 0 */
  struct _ds_option *back; /* bitsize 32, bitpos 32 */
  char *name; /* bitsize 32, bitpos 64 */
  char *str; /* bitsize 32, bitpos 96 */
  char *def_str; /* bitsize 32, bitpos 128 */
  int type; /* bitsize 32, bitpos 160 */
  int val; /* bitsize 32, bitpos 192 */
  int def_val; /* bitsize 32, bitpos 224 */
};
typedef struct _ds_option DS_OPTION;
typedef unsigned int size_t;
typedef void DS_FILE;
