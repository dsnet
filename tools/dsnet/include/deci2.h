 ../include/deci2.h:
typedef struct { /* size 8 */
  short unsigned int length; /* bitsize 16, bitpos 0 */
  short unsigned int reserved; /* bitsize 16, bitpos 16 */
  short unsigned int protocol; /* bitsize 16, bitpos 32 */
  unsigned char source; /* bitsize 8, bitpos 48 */
  unsigned char destination; /* bitsize 8, bitpos 56 */
} DECI2_HDR;
