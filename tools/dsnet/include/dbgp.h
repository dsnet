 ../include/dbgp.h:
typedef struct { /* size 8 */
  byte id; /* bitsize 8, bitpos 0 */
  byte group; /* bitsize 8, bitpos 8 */
  byte type; /* bitsize 8, bitpos 16 */
  byte code; /* bitsize 8, bitpos 24 */
  byte result; /* bitsize 8, bitpos 32 */
  byte count; /* bitsize 8, bitpos 40 */
  half unused; /* bitsize 16, bitpos 48 */
} DBGP_HDR;
typedef struct { /* size 68 */
  word major_ver; /* bitsize 32, bitpos 0 */
  word minor_ver; /* bitsize 32, bitpos 32 */
  word target_id; /* bitsize 32, bitpos 64 */
  word reserved1; /* bitsize 32, bitpos 96 */
  word mem_align; /* bitsize 32, bitpos 128 */
  word reserved2; /* bitsize 32, bitpos 160 */
  word reg_size; /* bitsize 32, bitpos 192 */
  word nreg; /* bitsize 32, bitpos 224 */
  word nbrkpt; /* bitsize 32, bitpos 256 */
  word ncont; /* bitsize 32, bitpos 288 */
  word nstep; /* bitsize 32, bitpos 320 */
  word nnext; /* bitsize 32, bitpos 352 */
  struct { /* size 8 */
    word mem_limit_align; /* bitsize 32, bitpos 0 */
    word mem_limit_size; /* bitsize 32, bitpos 32 */
  } v1; /* bitsize 64, bitpos 384 */
  struct { /* size 12 */
    word run_stop_state; /* bitsize 32, bitpos 0 */
    word hdbg_area_addr; /* bitsize 32, bitpos 32 */
    word hdbg_area_size; /* bitsize 32, bitpos 64 */
  } v3; /* bitsize 96, bitpos 448 */
} DBGP_CONF_DATA;
typedef struct { /* size 12 */
  byte space; /* bitsize 8, bitpos 0 */
  byte align; /* bitsize 8, bitpos 8 */
  half reserved; /* bitsize 16, bitpos 16 */
  word address; /* bitsize 32, bitpos 32 */
  word length; /* bitsize 32, bitpos 64 */
} DBGP_MEM;
typedef struct { /* size 4 */
  byte kind; /* bitsize 8, bitpos 0 */
  byte number; /* bitsize 8, bitpos 8 */
  half reserved; /* bitsize 16, bitpos 16 */
} DBGP_REG;
typedef struct { /* size 8 */
  word adr; /* bitsize 32, bitpos 0 */
  word cnt; /* bitsize 32, bitpos 32 */
} DBGP_BRKPT_DATA;
typedef struct { /* size 20 */
  word entry; /* bitsize 32, bitpos 0 */
  word gp; /* bitsize 32, bitpos 32 */
  word reserved1; /* bitsize 32, bitpos 64 */
  word reserved2; /* bitsize 32, bitpos 96 */
  word argc; /* bitsize 32, bitpos 128 */
} DBGP_EERUN;
typedef struct { /* size 12 */
  word flag; /* bitsize 32, bitpos 0 */
  word off; /* bitsize 32, bitpos 32 */
  word cnt; /* bitsize 32, bitpos 64 */
} DBGP_XGKT_CTL;
typedef struct { /* size 16 */
  word flag; /* bitsize 32, bitpos 0 */
  word length; /* bitsize 32, bitpos 32 */
  word offset; /* bitsize 32, bitpos 64 */
  word sequence; /* bitsize 32, bitpos 96 */
} DBGP_XGKT_DATA;
typedef struct { /* size 16 */
  half reserved; /* bitsize 16, bitpos 0 */
  half sbp; /* bitsize 16, bitpos 16 */
  half sbw; /* bitsize 16, bitpos 32 */
  half spsm; /* bitsize 16, bitpos 48 */
  half x; /* bitsize 16, bitpos 64 */
  half y; /* bitsize 16, bitpos 80 */
  half w; /* bitsize 16, bitpos 96 */
  half h; /* bitsize 16, bitpos 112 */
} DBGP_RDIMG;
typedef struct { /* size 16 */
  word length; /* bitsize 32, bitpos 0 */
  word sequence; /* bitsize 32, bitpos 32 */
  word reserved[2]; /* bitsize 64, bitpos 64 */
} DBGP_RDIMG_DATA;
typedef struct { /* size 20 */
  word type; /* bitsize 32, bitpos 0 */
  word number; /* bitsize 32, bitpos 32 */
  word mask; /* bitsize 32, bitpos 64 */
  word func; /* bitsize 32, bitpos 96 */
  word arg; /* bitsize 32, bitpos 128 */
} DBGP_EE_HANDLER_DATA;
typedef struct { /* size 4 */
  word count; /* bitsize 32, bitpos 0 */
} DBGP_EE_THREADLIST_HDR;
typedef struct { /* size 52 */
  word id; /* bitsize 32, bitpos 0 */
  word priority; /* bitsize 32, bitpos 32 */
  word status; /* bitsize 32, bitpos 64 */
  word cause; /* bitsize 32, bitpos 96 */
  word waitid; /* bitsize 32, bitpos 128 */
  word wakeupcount; /* bitsize 32, bitpos 160 */
  word counts; /* bitsize 32, bitpos 192 */
  word pc; /* bitsize 32, bitpos 224 */
  word sp; /* bitsize 32, bitpos 256 */
  word func; /* bitsize 32, bitpos 288 */
  word ra; /* bitsize 32, bitpos 320 */
  word reserved[2]; /* bitsize 64, bitpos 352 */
} DBGP_EE_THREADLIST_DATA;
typedef struct { /* size 4 */
  word id; /* bitsize 32, bitpos 0 */
} DBGP_EE_THREADID_DATA;
typedef struct { /* size 4 */
  word count; /* bitsize 32, bitpos 0 */
} DBGP_EE_THREADTCB_HDR;
typedef struct { /* size 768 */
  word id; /* bitsize 32, bitpos 0 */
  word currentpriority; /* bitsize 32, bitpos 32 */
  word status; /* bitsize 32, bitpos 64 */
  word cause; /* bitsize 32, bitpos 96 */
  word waitid; /* bitsize 32, bitpos 128 */
  word wakeupcount; /* bitsize 32, bitpos 160 */
  word counts; /* bitsize 32, bitpos 192 */
  word programcounter; /* bitsize 32, bitpos 224 */
  word stackpointer; /* bitsize 32, bitpos 256 */
  word func; /* bitsize 32, bitpos 288 */
  word args; /* bitsize 32, bitpos 320 */
  word argc; /* bitsize 32, bitpos 352 */
  word stack; /* bitsize 32, bitpos 384 */
  word stacksize; /* bitsize 32, bitpos 416 */
  word endofheap; /* bitsize 32, bitpos 448 */
  word option; /* bitsize 32, bitpos 480 */
  word gpReg; /* bitsize 32, bitpos 512 */
  word initpriority; /* bitsize 32, bitpos 544 */
  word sa; /* bitsize 32, bitpos 576 */
  word fcr31; /* bitsize 32, bitpos 608 */
  quad gpr[32]; /* bitsize 4096, bitpos 640 */
  twin hi; /* bitsize 64, bitpos 4736 */
  twin lo; /* bitsize 64, bitpos 4800 */
  twin hi1; /* bitsize 64, bitpos 4864 */
  twin lo1; /* bitsize 64, bitpos 4928 */
  word fpr[32]; /* bitsize 1024, bitpos 4992 */
  word facc; /* bitsize 32, bitpos 6016 */
  word reserved[3]; /* bitsize 96, bitpos 6048 */
} DBGP_EE_THREADTCB_DATA;
typedef struct { /* size 4 */
  word id; /* bitsize 32, bitpos 0 */
} DBGP_EE_SEMAID_DATA;
typedef struct { /* size 4 */
  word count; /* bitsize 32, bitpos 0 */
} DBGP_EE_SEMABLOCK_HDR;
typedef struct { /* size 24 */
  word id; /* bitsize 32, bitpos 0 */
  word count; /* bitsize 32, bitpos 32 */
  word maxcount; /* bitsize 32, bitpos 64 */
  word attr; /* bitsize 32, bitpos 96 */
  word option; /* bitsize 32, bitpos 128 */
  word numWaitThreads; /* bitsize 32, bitpos 160 */
} DBGP_EE_SEMABLOCK_DATA;
